<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2020-05-16 Sat 08:57 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Nabla backpropagation on time scales</title>
<meta name="generator" content="Org mode">
<meta name="author" content="Sasha Verona Malone">
<link rel="stylesheet" type="text/css" href="../assets/css/post-style.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2020 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">Nabla backpropagation on time scales</h1>
</header><nav id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#abstract">Abstract</a></li>
<li><a href="#introduction">Introduction</a>
<ul>
<li><a href="#example-digit-recognition">Example: digit recognition</a></li>
<li><a href="#how-neural-networks-work">How neural networks work</a></li>
<li><a href="#how-neural-networks-learn">How neural networks "learn"</a></li>
</ul>
</li>
<li><a href="#preliminaries">Preliminaries</a>
<ul>
<li><a href="#backpropagation-on-mathbbr">Backpropagation on \(\mathbb{R}\)</a></li>
<li><a href="#ordered-derivatives">Ordered derivatives</a></li>
<li><a href="#univariate-time-scales-calculus">Univariate time scales calculus</a></li>
<li><a href="#multivariate-time-scales-calculus">Multivariate time scales calculus</a></li>
</ul>
</li>
<li><a href="#main-results">Main results</a>
<ul>
<li><a href="#linear-regression-on-time-scales">Linear regression on time scales</a></li>
</ul>
</li>
<li><a href="#directions-for-future-research">Directions for future research</a>
<ul>
<li><a href="#clearer-exposition">Clearer exposition</a></li>
<li><a href="#universal-approximation">Universal approximation</a></li>
<li><a href="#recurrent-neural-networks">Recurrent neural networks</a></li>
</ul>
</li>
<li><a href="#references">References</a></li>
</ul>
</div>
</nav>
<article>
<div id="outline-container-org4c4fade" class="outline-2">
<h2 id="abstract">Abstract</h2>
<div class="outline-text-2" id="text-abstract">
<p>
<i>Neural networks</i> are a popular technique for machine learning problems
such as automated handwriting recognition, automated speech recognition,
and time-series forecasting. On these tasks, so-called <i>deep neural
networks</i> [Schmidhuber 2015] and <i>convolutional neural networks</i>
[Ciresan et al. 2011] have performed as well or better than the best
known non-neural-network statistical learning techniques. A typical
neural network numerically solves an extremization problem using
gradient descent in a high-dimensional Euclidean space using an
algorithm known as <i>backpropagation</i>. In this paper, we provide an
exposition of (feedforward) neural networks and backpropagation on
\(\mathbb{R}^n\), such as may be found in [Nielsen 2015]; and we
generalize these concepts from \(\mathbb{R}^n\) to a product of <i>time
scales</i>, or closed subsets of \(\mathbb{R}\) which may be partially
discrete and partially continuous.
</p>

<p>
Most of the main results were already derived in [Seiffert &amp; Wunsch
2010] for delta derivatives; we present what are mostly the dual cases
for nabla derivatives.
</p>
</div>
</div>

<div id="outline-container-org6b3e382" class="outline-2">
<h2 id="introduction">Introduction</h2>
<div class="outline-text-2" id="text-introduction">
<p>
Neural networks perform <i>supervised learning tasks</i>, meaning that they
"learn" to emulate a desired set of outputs from a given set of
already-classified <i>training examples</i>.
</p>

<p>
If we think of the aforementioned "desired set of outputs" as a function
\(f : \mathbb{R}^M \to \mathbb{R}^N\), then a good neural network
approximates \(f\) accurately by a function
\(\hat{f} : \mathbb{R}^M \to \mathbb{R}^N\).
</p>

<p>
We measure the network's performance by a positive <i>loss function</i>
\(L : \mathbb{R}^N \to [0, \infty)\), which we want the network to
minimize. Typically \(L\) is the sum of squared errors
</p>

<p>
\[ \begin{aligned}
    L(\hat{f}(I)) = \frac{1}{2} \sum_{k=1}^N (f(I)_k - \hat{f}(I)_k)^2,
\end{aligned} \]
</p>

<p>
where \(f(I)\) is the "expected" response and \(\hat{f}(I)\) is the
network's actual response.
</p>
</div>

<div id="outline-container-org9b1993d" class="outline-3">
<h3 id="example-digit-recognition">Example: digit recognition</h3>
<div class="outline-text-3" id="text-example-digit-recognition">
<p>
One of the canonical problems for neural network learning is <i>digit
recognition</i>: given an image that represents a (handwritten) numeral
from 0 to 9, we wish to classify it appropriately. This section gives a
brief rigorous specification of the problem as it is usually stated.
</p>

<p>
The MNIST dataset, which consists of 60,000 greyscale images that are
each 28 pixels square, is traditionally used for training. Such an image
has a total of \(28^2 = 784\) pixels, each of which has an intensity
between 0 (black) and 1 (white.) Thus it can be represented as an
element of \([0, 1]^{784}\), and we expect the network to output its
relative confidence that the image represents a 0, 1, &#x2026;, 9 as a
probability (so an element of \([0, 1]^{10}\) whose sum is 1.) Thus the
network's job is to approximate an unknown function
\(f : \mathbb{R}^{784} \to \mathbb{R}^{10}\) whose value at each image in
the training set is known.
</p>

<p>
For instance, we expect that an image \(I \in \mathbb{R}^{784}\) which
represents a numeral 0 should have \(f(I) = [1, 0, 0, \dots, 0]^T\). If
the network instead responded with
\(\hat{f}(I) = [0.9, 0.05, 0.05, 0, \dots, 0]^T\), and \(L\) was defined as
above, we would penalize the network
\[L(\hat{f}(I)) = \frac{1}{2}\left((0.9 - 1)^2 + (0.05 - 0)^2 + (0.05 - 0)^2\right) = .000525\]
points.
</p>
</div>
</div>

<div id="outline-container-org8303391" class="outline-3">
<h3 id="how-neural-networks-work">How neural networks work</h3>
<div class="outline-text-3" id="text-how-neural-networks-work">
<p>
The structure of a neural network is more or less universally depicted
using a directed graph similar to the one at right, in which circular
nodes are arranged into parallel "layers" (columns) and connected by
edges.
</p>

<p>
Computation in neural networks proceeds layer-by-layer from left to
right. The leftmost (first) layer is called the <i>input</i> layer, the
rightmost is called the <i>output</i> layer, and all other layers are said to
be <i>hidden</i>.
</p>

<p>
At each node \(k\), the network computes a value \[\begin{aligned}
    x_k = \psi_k\left(b_k + \sum_{i \to k} w_ix_i\right),\end{aligned}\]
where
</p>

<ul class="org-ul">
<li>the sum runs over all nodes \(i\) that "feed into" (are direct
predecessors of) \(k\);</li>

<li>\(b_k\) and \(w_k\) are real-valued parameters of node \(k\), called
respectively the <i>bias</i> and <i>weight</i>;</li>

<li>and \(\psi_k : \mathbb{R} \to \mathbb{R}\) is a so-called <i>activation
function</i>. Typically \(\psi\) will be a bounded nonlinear function for
nodes in hidden layers and the identity for the output layer. Common
choices of \(\psi\) include the hyperbolic tangent and the logistic
function \(e^x/(1 + e^x)\).</li>
</ul>

<p>
In this paper, we will consider only <i>feedforward</i> neural networks,
which require the directed graph to be acyclic. (This assumption is
relaxed in so-called <i>recurrent</i> neural networks; see Section \(6.2\) for
more.) Also, we will assume for simplicity that every node in the $n$th
layer feeds into every node in the $n + 1$st layer.
</p>

<p>
We will use the label \(\alpha\) for the input layer; that is, we denote
the values of the nodes in the input layer
\(x_{\alpha{}1}, \dots, x_{\alpha{}M}\). Similarly, we label the hidden
layers \(1\), \(2\), &#x2026;, \(H\), and we label the output layer \(o\). When a sum
is understood to run over all nodes in a given layer, we will be lax
with the limits of summation, e.g., $&sum;<label for='af7062be' class='margin-toggle sidenote-number'></label><input id='af7062be' type='checkbox' class='margin-toggle' /><span class='sidenote'>l</span></label> w<label for='97e251b6' class='margin-toggle sidenote-number'></label><input id='97e251b6' type='checkbox' class='margin-toggle' /><span class='sidenote'>Hl</span></label>x<label for='b3ae29ce' class='margin-toggle sidenote-number'></label><input id='b3ae29ce' type='checkbox' class='margin-toggle' /><span class='sidenote'>Hl</span></label>.$
</p>

<p>
Under these assumptions, we can rewrite equation (network)
as
</p>

<p>
\[\begin{aligned}
    x_{ij} = \psi_{ij}\left(b_{ij} + \sum_k w_{(i - 1),k}x_{(i - 1),k}\right)
\end{aligned}\]
</p>

<p>
where for instance \(x_{ij}\) denotes the value computed at the $j$th node
(top-to-bottom) in the $i$th layer (left-to-right.)
</p>

<p>
Using the foregoing notation, we may also write the expression for the
loss function, equation (loss), as \[\begin{aligned}
    L(\hat{f}(I)) = L(x_{o1}, \dots, x_{oN}) = \frac{1}{2} \sum_{k = 1}^N (y_k - x_{ok})^2
\end{aligned}\] where we have written \(f(I) = [y_1, \dots, y_N]^T\) and
\(\hat{f}(I) = [x_{o1}, \dots, x_{oN}]^T\). This notation will be more
convenient in the sequel.
</p>
</div>
</div>

<div id="outline-container-org8cf390e" class="outline-3">
<h3 id="how-neural-networks-learn">How neural networks "learn"</h3>
<div class="outline-text-3" id="text-how-neural-networks-learn">
<p>
In principle, neural networks learn by a process similar to humans: they
attempt the task they are intended to perform many times, making
appropriate adaptations when they make a mistake. When we decide to use
neural network learning for some problem, we usually specify only the
network topology; feed it many, many examples; and allow backpropagation
to find the right weights and biases for us. This process is known as
<i>training</i>.
</p>
</div>
</div>
</div>

<div id="outline-container-org1a4b12c" class="outline-2">
<h2 id="preliminaries">Preliminaries</h2>
<div class="outline-text-2" id="text-preliminaries">
<p>
In this section we present the necessary prerequisites for appreciating
the main results: the backpropagation algorithm on Euclidean domains
(\(\mathbb{R}^n\)), following chapter 2 of [Nielsen 2015], and the
generalization to time scales calculus of the necessary concepts from
multivariable differential calculus, following [Bohner &amp; Guseinov 2004].
</p>
</div>

<div id="outline-container-org5b65e27" class="outline-3">
<h3 id="backpropagation-on-mathbbr">Backpropagation on \(\mathbb{R}\)</h3>
<div class="outline-text-3" id="text-backpropagation-on-mathbbr">
<p>
As already said, the backpropagation algorithm is responsible for
finding the weights and biases that minimize the loss function. This is
done iteratively by <i>gradient descent</i>. Suppose we have already computed
the network's output \(\hat{f}(I)\) for some training example \(I\). Then
for each node \(x_k\), we compute the partials
</p>

<p>
\[L_{w_k} = \frac{\partial L}{\partial w_k}, L_{b_k} = \frac{\partial L}{\partial b_k}\]
</p>

<p>
and update the parameters according to the <i>delta rule</i>
</p>

<p>
\[\begin{aligned}
    \Delta w_k &= -\beta L_{w_k}, \\
    \Delta b_k &= -\beta L_{b_k},
\end{aligned}\]
</p>

<p>
where \(\beta > 0\) is a constant (referred to as the <i>learning rate</i>.)
</p>

<p>
Backpropagation computes all the partials simultaneously by recursive
total differentiation of []. For simplicity we
focus on the partial with respect to a single parameter. Suppose we want
to compute \(L_{w_k} = L_{w_{ij}}\) for some node \(k\) (the computation of
\(L_{b_k}\) will proceed in an entirely similar way.) Recall that \(L\)
takes the values of nodes in the output layer as its inputs; so by the
chain rule,
</p>

<p>
\[\begin{aligned}
    \frac{\partial L}{\partial w_{ij}} &= \sum_k \frac{\partial L}{\partial x_{ok}} \frac{\partial x_{ok}}{\partial w_{ij}} = \sum_k (y_k - x_{ok}) \cdot \frac{\partial x_{ok}}{\partial w_{ij}}.
\end{aligned}\]
</p>

<p>
In turn, since \(x_{ok}\) depends on every node in the last hidden layer,
as well as those nodes' weights and its own bias, we have
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{ok}}{\partial w_{ij}} &= \frac{\partial x_{ok}}{\partial b_{ok}} \frac{\partial b_{ok}}{\partial w_{ij}} + \sum_l \left(\frac{\partial x_{ok}}{\partial x_{Hl}} \frac{\partial x_{Hl}}{\partial w_{ij}} + \frac{\partial x_{ok}}{\partial w_{Hl}} \frac{\partial w_{Hl}}{\partial w_{ij}}\right).
\end{aligned}\]
</p>

<p>
For the moment, we assume that \(w_{ij}\) is independent of all the other
weights and all the biases. Thus this equation becomes
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{ok}}{\partial w_{ij}} &= \sum_l \frac{\partial x_{ok}}{\partial x_{Hl}} \frac{\partial x_{Hl}}{\partial w_{ij}}.
\end{aligned}\]
</p>

<p>
Similarly, we have
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{Hk}}{\partial w_{ij}} &= \sum_l \frac{\partial x_{Hk}}{\partial x_{(H - 1),l}} \frac{\partial x_{(H - 1),l}}{\partial w_{ij}}.
\end{aligned}\]
</p>

<p>
In this way, we work our way back (or <i>backpropagate</i>) through the
network until we reach the layer that directly depends on \(w_{ij}\), at
which point the terms corresponding to weight partials are no longer all
zero, and we can compute directly
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{(i + 1),k}}{\partial w_{ij}} &= \frac{\partial}{\partial w_{ij}} \left[\psi_{(i+1),k}\left(b_{(i + 1),k} + \sum_l w_{il}x_{il}\right)\right] \\
                                                   &= \psi'_{(i+1),k}\left(b_{(i + 1),k} + \sum_l w_{il}x_{il}\right) \cdot x_{ij}.
\end{aligned}\]
</p>

<p>
The weight can now be adjusted according to
</p>
</div>
</div>

<div id="outline-container-org25f2ae5" class="outline-3">
<h3 id="ordered-derivatives">Ordered derivatives</h3>
<div class="outline-text-3" id="text-ordered-derivatives">
<p>
In the previous section we assumed that \(w_{ij}\) was independent of all
the other \(w_{i'j'}\) and also of the biases \(b_{i'j'}\). This is okay for
the foregoing derivation, in which we assumed all the other parameters
were being held constant. However, actual implementations of
backpropagation update all the parameters simultaneously. Hence this
assumption is no longer valid; in particular, the change in the weight
(and bias) of a given node affects all of its successors.
</p>

<p>
To remedy this, [Werbos 1990] introduced an "ordered chain rule" that
accounts for these indirect effects. We present it below without proof.
A time scales version (for delta derivatives) is proven and rigorously
exposited in [Seifertt &amp; Wunsch 2010] as Theorem 2.
</p>

<p>
Define the variables \(x_i = f(x_1, \dots, x_{i - 1})\) recursively. Then
the "ordered derivative" of \(x_j\) with respect to \(x_i\) is given by
</p>

<p>
\[\begin{aligned}
          \frac{\partial^+ x_j}{\partial x_i} = \frac{\partial x_j}{\partial x_i} + \sum_{k = i, \dots, j} \frac{\partial^+ x_j}{\partial x_k} \frac{\partial x_k}{x_i}.
  \end{aligned}\]
</p>

<p>
The sum in the definition of the ordered derivative accounts for the
indirect effects incurred by updates in the variables computed "after"
\(x_i\) but "before" \(x_j\); that is, the variables through which \(x_j\)
depends transitively on \(x_i\).
</p>
</div>
</div>

<div id="outline-container-org4707454" class="outline-3">
<h3 id="univariate-time-scales-calculus">Univariate time scales calculus</h3>
<div class="outline-text-3" id="text-univariate-time-scales-calculus">
<p>
This section consists of a number of standard definitions which will be
immediately generalized. For a more complete treatment of
single-variable time scales calculus in which these results appear, see
[Bohner &amp; Peterson 2012].
</p>

<p>
A <i>time scale</i> is a nonempty closed subset of \(\mathbb{R}\).
</p>

<p>
In the sequel, we use \(\mathbb{T}\) to refer to an arbitrary time scale.
</p>

<p>
We define the <i>backward jump function</i>
\(\rho : \mathbb{T} \to \mathbb{T}\) by
</p>

<p>
\[\rho(t) \vcentcolon= \sup{ \mathbb{T} \cap (\infty, t) }\]
</p>

<p>
and the <i>backward graininess function</i>
\(\nu : \mathbb{T} \to [0, \infty)\) by
</p>

<p>
\[\nu(t) \vcentcolon= t - \rho(t).\]
</p>

<p>
Let \(t \in \mathbb{T}\). If \(\nu(t) \> 0\), we say \(t\) is <i>(left)
scattered</i>. Otherwise we say \(t\) is <i>(left) dense</i>.
</p>

<p>
Let \(t \in \mathbb{T}\) and let \(f : \mathbb{T} \to \mathbb{R}\). If the
limit \[\begin{aligned}
      \lim_{s \to t, s \not = \rho(t)} \frac{f(\rho(t)) - f(s)}{\rho(t) - s} = f^\nabla(t)
  \end{aligned}\] exists as a finite number, we say that \(f\) is <i>nabla
differentiable at \(t\)</i> and call \(f^\nabla(t)\) the <i>nabla derivative of
\(f\) at \(t\)</i>.
</p>

<p>
The following generalization of the univariate chain rule will be
necessary later. It appears for delta derivatives in [Bohner &amp; Peterson
2012] as Theorem 1.90.
</p>

<p>
Suppose \(f : \mathbb{R} \to \mathbb{R}\) is \(C^1\). Suppose
\(g : \mathbb{T} \to \mathbb{R}\) is nabla differentiable. Then
\(f \circ g : \mathbb{T} \to \mathbb{R}\) is nabla differentiable and
</p>

<p>
\[\begin{aligned}
          (f \circ g)^\nabla(t) = \int_0^1 f'(g(t) + h\nu(t)g^\nabla(t)) ~dh.
      \end{aligned}\]
</p>

<p>
The definitions of the dual concepts (<i>e.g.</i>, forward jump function,
right scattered point, delta derivative) may be easily inferred. We will
not need them in the remainder of this paper.
</p>
</div>
</div>

<div id="outline-container-org63f9ae4" class="outline-3">
<h3 id="multivariate-time-scales-calculus">Multivariate time scales calculus</h3>
<div class="outline-text-3" id="text-multivariate-time-scales-calculus">
<p>
We now consider products of time scales
\(\Lambda^n = \prod_{i=1}^n \mathbb{T}_i\). We regard these as time scales
in their own right. Note that each component time scale has its own
(possibly distinct) backward jump \(\rho_i\) and backward graininess
\(\nu_i\).
</p>

<p>
Suppose \(f\) is defined on \(\Lambda^n\) and
\(\mathbf{t} = [t_1, \dots, t_n]^T \in \Lambda^n\). We define the backward
jump on \(\Lambda^n\) as follows.
</p>

<p>
\[\begin{aligned}
        \rho(\mathbf{t}) &= [\rho_1(t_1), \dots, \rho_i(t_i), \dots, \rho_n(t_n)]^T, \\
        f^{\rho}(\mathbf{t}) &= f(\rho_1(t_1), \dots, \rho_i(t_i), \dots, \rho_n(t_n)).
\end{aligned}\]
</p>

<p>
For compactness, we also make use of the following abuse of notation:
</p>

<p>
\[\begin{aligned}
        \rho_i(\mathbf{t}) &= [t_1, \dots, \rho_i(t_i), \dots, t_n]^T, \\
        f^{\rho_i}(\mathbf{t}) &= f(t_1, \dots, \rho_i(t_i), \dots, t_n).
\end{aligned}\]
</p>

<p>
The following definitions are given (for delta derivatives) in [Bohner &amp;
Guseinov 2004].
</p>

<p>
Let \(f : \Lambda^n \to \mathbb{R}\). Let
\(\mathbf{t} = [t_1, \dots, t_n]^T \in \Lambda^n\). If the limit
</p>

<p>
\[\begin{aligned}
        \lim_{s_i \to t_i, s_i \not = \rho_i(t_i)} \frac{f^{\rho_i}(\mathbf{t}) - f(t_1, \dots, s_i, \dots, t_n)}{\rho_i(t_i) - s_i} = \frac{\partial f}{\nabla_i t_i}(\mathbf{t})
\end{aligned}\]
</p>

<p>
exists as a finite number, we say that \(f\) is <i>nabla differentiable in
the $i$th variable at \(\mathbf{t}\)</i> and call
\(\displaystyle\frac{\partial f}{\nabla_i t_i}(\mathbf{t})\) the <i>nabla
derivative of \(f\) with respect to \(t_i\) at \(\mathbf{t}\)</i>.
</p>

<p>
To generalize the backpropagation algorithm, we will require the total
derivative of some functions to be well-defined. The following two
definitions provide an appropriate sufficient condition; a more explicit
(and lengthier) version that does not rely on vectors appears as
Definitions 2.1 and 2.3 in [Bohner &amp; Guseinov 2004].
</p>

<p>
Let \(f : \Lambda^n \to \mathbb{R}\). Let \(\mathbf{t} \in \Lambda^n\). Fix
a positive \(\epsilon\). If there exists a vector
\(\mathbf{A}(\mathbf{t}) = \mathbf{A} \in \mathbb{R}^n\) such that for any
\(\mathbf{s} \in \Lambda^n\) with \(\Vert\mathbf{t} - \mathbf{s}\Vert\) less
than \(\epsilon\) there are vectors \(\mathbf{a}\),
\(\mathbf{b}_{1,\dots,n} \in \mathbb{R}^n\) such that
</p>

<p>
\[\begin{aligned}
        f(\mathbf{t}) - f(\mathbf{s}) &= (\mathbf{A} + \mathbf{a})\cdot(\mathbf{t} - \mathbf{s})
\end{aligned}\]
</p>

<p>
and for \(j = 1, \dots, n\)
</p>

<p>
\[\begin{aligned}
        f^{\rho_j}(\mathbf{t}) - f(\mathbf{s}) &= (\mathbf{A} + \mathbf{b}_j) \cdot (\rho_j(\mathbf{t}) - \mathbf{s}),
    \end{aligned}\]
</p>

<p>
and further
\(\lim_{\mathbf{s} \to \mathbf{t}} \mathbf{a} = \lim_{\mathbf{s} \to \mathbf{t}} \mathbf{b}_j = \mathbf{0}\),
we say \(f\) is <i>completely nabla differentiable at \(\mathbf{t}\)</i>.
</p>

<p>
In this definition the vector \(\mathbf{A}\) plays the role of the
gradient of \(f\); that is, if it exists, its $i$th component is equal to
\(\frac{\partial f}{\nabla_i t_i}(\mathbf{t})\).
</p>

<p>
Suppose \(f : \Lambda^n \to \mathbb{R}\) is completely nabla
differentiable at \(\mathbf{t} \in \Lambda^n\). Fix a positive \(\epsilon\).
If there exists a vector
\(\mathbf{B}(\mathbf{t}) = \mathbf{B} \in \mathbb{R}^n\) which agrees with
\(\mathbf{A}\) (as defined above) in the $i$th component, viz.
</p>

<p>
\[\begin{aligned}
        \mathbf{B}_i &= \mathbf{A}_i = \frac{\partial f}{\nabla_i t_i}(\mathbf{t}),
\end{aligned}\] and for any \(\mathbf{s} \in \Lambda^n\) with
\(\Vert\mathbf{t} - \mathbf{s}\Vert\) less than \(\epsilon\) there exists
\(\mathbf{v} \in \mathbb{R}^n\) such that
\(\displaystyle\lim_{\mathbf{s} \to \mathbf{t}} \mathbf{v} = \mathbf{0}\)
and
</p>

<p>
\[\begin{aligned}
        f^\rho(\mathbf{t}) - f(\mathbf{s}) &= (\mathbf{B} + \mathbf{v}) \cdot (\rho(\mathbf{t}) - \mathbf{s}),
    \end{aligned}\]
</p>

<p>
then we say \(f\) is <i>$&rho;<label for='faa5f3b5' class='margin-toggle sidenote-number'></label><input id='faa5f3b5' type='checkbox' class='margin-toggle' /><span class='sidenote'>i</span></label>$-completely nabla differentiable at
\(\mathbf{t}\)</i>.
</p>

<p>
If this condition holds, the components of \(\mathbf{B}\) other than the
$i$th are given by
</p>

<p>
\[\begin{aligned}
    \mathbf{B}_j = \frac{\partial f}{\nabla_j t_j}(\rho_i(\mathbf{t})).\end{aligned}\]
</p>

<p>
The final definition we need will be used in the proof of the main
theorem.
</p>

<p>
Let \(f : \Lambda^n \to \mathbb{T}\). If
\(f^{\rho_{\Lambda^n}}(\mathbf{t}) = \rho_{\mathbb{T}}(f(\mathbf{t}))\),
we say that \(f\) is <i>backward jump commutative</i> or <i>$&rho;$-commutative</i>.
</p>
</div>
</div>
</div>

<div id="outline-container-orgba24c55" class="outline-2">
<h2 id="main-results">Main results</h2>
<div class="outline-text-2" id="text-main-results">
<p>
We have now established the necessary scaffolding to generalize the main
theorem: the standard multivariate chain rule, the delta derivative
analog of which is proven in [Bohner &amp; Guseinov 2004], Theorem 7.1 (in
two dimensions) and [Seifertt &amp; Wunsch 2010], Theorem 1 (in \(n\)
dimensions.) We closely follow the latter proof here.
</p>

<p>
The definition of the functions \(u_i\) in the theorem statement requires
a little care, since previously we defined nabla differentiability only
for functions whose image is \(\mathbb{R}\). We assume throughout this
discussion that the image of \(u_i\),
\(u_i(\Lambda^n) = \mathbb{T}_i \subseteq \mathbb{R}\), is a time scale,
and is more specifically (a subset of) the $i$th component of
\(\Lambda^n\), so that the composite function \(F\) is well-defined.
</p>

<p>
Suppose \(f : \Lambda^n \to \mathbb{R}\) is $&rho;<label for='1ededf3f' class='margin-toggle sidenote-number'></label><input id='1ededf3f' type='checkbox' class='margin-toggle' /><span class='sidenote'>1</span></label>$-completely nabla
differentiable. Suppose \(u_i : \Lambda^n \to \mathbb{R}\) is nabla
differentiable and $&rho;<label for='14d33550' class='margin-toggle sidenote-number'></label><input id='14d33550' type='checkbox' class='margin-toggle' /><span class='sidenote'>i</span></label>$-commutative for \(i = 1, \dots, n\).
</p>

<p>
Define \(F : \Lambda^n \to \mathbb{R}\) by
\[F(\mathbf{t}) = f(\mathbf{u}(\mathbf{t})) = f(u_1(\mathbf{t}), \dots, u_n(\mathbf{t})).\]
The nabla derivative of \(F\) exists and is given by
</p>

<p>
\[\begin{aligned}
        F^\nabla(\mathbf{t}) &= \frac{\partial F}{\nabla_1 u_1}(\mathbf{t}) \cdot u_1^\nabla(\mathbf{t}) + \sum_{k = 2}^n \frac{\partial F}{\nabla_k u_k}(\rho_1(\mathbf{t})) \cdot u_k^\nabla(\mathbf{t}).
    \end{aligned}\]
</p>

<p>
Let \(\epsilon > 0\). Let \(\mathbf{t} \in \Lambda^n\) be given. A bit
loosely, we write \[\begin{aligned}
        F^\nabla(\mathbf{t}) &= \lim_{\Vert\mathbf{s} - \mathbf{t}\Vert \to 0, \mathbf{s} \not = \rho(\mathbf{t})} \frac{F(\rho(\mathbf{t})) - F(\mathbf{s})}{\Vert\rho(\mathbf{t}) - \mathbf{s}\Vert}.
\end{aligned}\] For simplicity, we will work with the numerator
\[\begin{aligned}
        F(\rho(\mathbf{t})) - F(\mathbf{s}) &= f(u_1(\rho(\mathbf{t})), \dots, u_n(\rho(\mathbf{t}))) - f(u_1(\mathbf{s}), \dots, u_n(\mathbf{s}))
\end{aligned}\] and pass to the limit at the end. Therefore let
\(\mathbf{s}\) be fixed with \(\Vert\mathbf{s} - \mathbf{t}\Vert\) less than
\(\epsilon\). Since the \(u_i\) are $&rho;$-commutative, we have
</p>

<p>
\[\begin{aligned}
        F(\rho(\mathbf{t})) - F(\mathbf{s}) &= f(\rho_1(u_1(\mathbf{t})), \dots, \rho_n(u_n(\mathbf{t}))) - f(u_1(\mathbf{s}), \dots, u_n(\mathbf{s})) \\
                                            &= F^\rho(\mathbf{t}) - F(\mathbf{s}).
                                            \end{aligned}\] Since \(F\) is
$&rho;<label for='25288e36' class='margin-toggle sidenote-number'></label><input id='25288e36' type='checkbox' class='margin-toggle' /><span class='sidenote'>1</span></label>$-completely nabla differentiable, there exist
\(\mathbf{B}, \mathbf{v} \in \mathbb{R}^n\) such that
\(\lim_{\mathbf{s} \to \mathbf{t}} \mathbf{v} = \mathbf{0}\) and
</p>

<p>
\[\begin{aligned}
        F^\rho(\mathbf{t}) - F(\mathbf{s}) &= (\mathbf{B} + \mathbf{v}) \cdot (\rho(\mathbf{u}(\mathbf{t})) - \mathbf{u}(\mathbf{s})) \\
                                           &= \sum_{k = 1}^n B_k(\rho_k(u_k(\mathbf{t})) - u_k(\mathbf{s})) \\
                                           &\qquad+ \mathbf{v} \cdot (\rho(\mathbf{u}(\mathbf{t})) - \mathbf{u}(\mathbf{s})).
\end{aligned}\]
</p>

<p>
By (\ref{bi}) and (\ref{bj}), this is
</p>

<p>
\[\begin{aligned}
&= \left(\frac{\partial F}{\nabla_1 u_1}(\mathbf{t}) \right) (\rho_1(u_1(\mathbf{t})) - \mathbf{s}) \\
&\qquad+ \sum_{k=2}^n \left(\frac{\partial F}{\nabla_k u_k}(\rho_1(\mathbf{t}))\right)(\rho_k(u_k(\mathbf{t})) - u_k(\mathbf{s})) \\
&\qquad+ \mathbf{v} \cdot (\rho(\mathbf{u}(\mathbf{t})) - \mathbf{u}(\mathbf{s})).
\end{aligned}\]
</p>

<p>
Now applying the $&rho;$-commutativity condition in the other direction
gives
</p>

<p>
\[\begin{aligned}
&= \left(\frac{\partial F}{\nabla_1 u_1}(\mathbf{t}) \right) (u_1(\rho(\mathbf{t})) - \mathbf{s}) \\
&\qquad+ \sum_{k=2}^n \left(\frac{\partial F}{\nabla_k u_k}(\rho_1(\mathbf{t}))\right)(u_k(\rho(\mathbf{t})) - u_k(\mathbf{s})) \\
&\qquad+ \mathbf{v} \cdot (\mathbf{u}(\rho(\mathbf{t})) - \mathbf{u}(\mathbf{s})).
\end{aligned}\]
</p>

<p>
As earlier discussed, we divide throughout by
\(\Vert\rho(\mathbf{t}) - \mathbf{s}\Vert\) and take the limit as
\(\Vert\mathbf{t} - \mathbf{s}\Vert\) approaches \(0\), which gives
\[\begin{aligned}
        F^\nabla(\mathbf{t}) &= \frac{\partial F}{\nabla_1 u_1}(\mathbf{t}) \cdot u_1^\nabla(\mathbf{t}) + \sum_{k = 2}^n \frac{\partial F}{\nabla_k u_k}(\rho_1(\mathbf{t})) \cdot u_k^\nabla(\mathbf{t}),
\end{aligned}\] which was to be shown.
</p>

<p>
Now we generalize the earlier exposition of the backpropagation
algorithm, viz. -
</p>

<p>
Suppose we have a neural network governed by equations
 and j with the
single difference that the domain of \(x_{ij}\) (and range of the
activation function \(\psi_{ij}\)) is a time scale \(\mathbb{T}\).
Throughout we will assume that this time scale is the same for all
nodes, so independent of \(i\) and \(j\). We therefore write
\(\Lambda^n = \mathbb{T}^n\).
</p>

<p>
The following discussion follows [Seifertt &amp; Wunsch 2010], section 4. We
would like to compute the partial nabla derivative
\(\displaystyle\frac{\partial L}{\nabla w_{ij}}\) for some node \(ij\). By
the chain rule for \(n\) variables, we have
</p>

<p>
\[\begin{aligned}
    \frac{\partial L}{\nabla w_{ij}}(\mathbf{t}) &= \frac{\partial L}{\nabla x_{o1}}(\mathbf{t}) \cdot \frac{\partial x_{o1}}{\nabla w_{ij}}(\mathbf{t}) + \sum_{k > 1} \frac{\partial L}{\nabla x_{ok}}(\rho_1(\mathbf{t})) \frac{\partial x_{ok}}{\nabla w_{ij}}(\mathbf{t}).
\end{aligned}\]
</p>

<p>
Similarly,
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{ok}}{\nabla w_{ij}}(\mathbf{t}) &= \frac{\partial x_{ok}}{\nabla x_{H1}}(\mathbf{t}) \cdot \frac{\partial x_{H1}}{\nabla w_{ij}}(\mathbf{t}) + \sum_{l > 1} \frac{\partial x_{ok}}{\nabla x_{Hl}}(\rho_1(\mathbf{t})) \frac{\partial x_{Hl}}{\nabla w_{ij}}(\mathbf{t}).
\end{aligned}\]
</p>

<p>
When we reach the layer that directly depends on \(w_{ij}\), we have to
compute
</p>

<p>
\[\begin{aligned}
    \frac{\partial x_{(i + 1),k}}{\nabla w_{ij}} &= \left(\int_0^1 \psi_{(i+1),k}'\left(b_{(i+1), k} + \sum_l w_{il} x_{il} + h\nu(t)x_{ij} \right)~dh \right) \cdot x_{ij}.
\end{aligned}\]
</p>
</div>

<div id="outline-container-org8f035b5" class="outline-3">
<h3 id="linear-regression-on-time-scales">Linear regression on time scales</h3>
<div class="outline-text-3" id="text-linear-regression-on-time-scales">
<p>
Several other statistical learning techniques admit representations as
neural networks. As a simple application of the results just derived, we
recast multiple linear regression, one of the simplest possible models,
as a neural network, and work through the backpropagation algorithm on
time scales.
</p>

<p>
Suppose we have a neural network with no hidden layers, a single output
node, and \(M\) input nodes. Further, suppose all the activation functions
are the identity. (On \(\mathbb{R}\), by taking the activation function at
the output to be, e.g., the hyperbolic tangent, we obtain <i>logit
regression</i>.)
</p>

<p>
The network equation  becomes
</p>

<p>
\[\begin{aligned}
    x_{o1} = y &= b_y + \sum_{k = 1}^M w_{\alpha k}x_{\alpha k} \\
               &= b_y + \sum_{k = 1}^M w_kx_k.
\end{aligned}\]
</p>

<p>
Suppose we use the loss function  and observe an
error \(L(\hat{f}(I)) = L = (y - \hat{y})^2/2\). To update the parameters,
we need to compute
</p>

<p>
\[\begin{aligned}
    \frac{\partial L}{\nabla w_k}(I) &= \frac{\partial L}{\nabla y}(I) \frac{\partial y}{\nabla w_k}(I) \\
                                     &= -\frac{y - \hat{y} + \nu(y - \hat{y})}{2} \cdot \frac{\partial y}{\nabla w_k}(I)
                                     \end{aligned}\] Since
\(\psi_k' = 1\), we have
</p>

<p>
\[\begin{aligned}
    \frac{\partial y}{\nabla w_k} &= x_k,
    \frac{\partial L}{\nabla w_k} &= \frac{y - \hat{y} + \nu(y - \hat{y})}{2} x_k.\end{aligned}\]
</p>

<p>
The weight is now updated as
$\displaystyle&Delta; w<label for='28cd3c2b' class='margin-toggle sidenote-number'></label><input id='28cd3c2b' type='checkbox' class='margin-toggle' /><span class='sidenote'>k</span></label> = -&beta; \frac{\partial L}{\nabla w_k}.$ If
\(\beta = 1\) and \(\mathbb{T} = \mathbb{R}\), then this becomes
$\displaystyle&Delta; w<label for='1fa2a618' class='margin-toggle sidenote-number'></label><input id='1fa2a618' type='checkbox' class='margin-toggle' /><span class='sidenote'>k</span></label> = (y - \hat{y})x<label for='795af31e' class='margin-toggle sidenote-number'></label><input id='795af31e' type='checkbox' class='margin-toggle' /><span class='sidenote'>k</span></label>.$
</p>
</div>
</div>
</div>

<div id="outline-container-orgf1177cf" class="outline-2">
<h2 id="directions-for-future-research">Directions for future research</h2>
<div class="outline-text-2" id="text-directions-for-future-research">
</div>

<div id="outline-container-org38b340b" class="outline-3">
<h3 id="clearer-exposition">Clearer exposition</h3>
<div class="outline-text-3" id="text-clearer-exposition">
<p>
When discussing the mathematics of neural networks, it is easy to get
lost while keeping track of the various indices under study. The paper
[Seifertt &amp; Wunsch 2010] compounds this problem by stating the
$n$-variable chain rule in full generality, which almost by necessity
skims over some delicate considerations regarding, <i>e.g.</i>, the
well-definedness of the composite function \(F\). In this paper, I have
tried to clean up their exposition in hopes of greater readability
</p>

<p>
The standard treatment of the total derivative in \(\mathbb{R}^n\) is as a
certain linear mapping (see for instance [Spivak 1965].) This allows
full "vectorization" of the appropriate definitions, which eliminates
complex indicial notation. An equivalent treatment for products of time
scales \(\Lambda^n\) would be highly desirable, but is outside the scope
of this paper.
</p>
</div>
</div>

<div id="outline-container-org316a3c0" class="outline-3">
<h3 id="universal-approximation">Universal approximation</h3>
<div class="outline-text-3" id="text-universal-approximation">
<p>
On continuous domains, the power of feedforward neural networks is
demonstrated by the following result, which implies that neural networks
can (theoretically) be used to model any continuous function in
Euclidean space. This result was presented in various slightly different
forms by several papers in the late 1980s and early 1990s; we follow the
exposition in [Hornik 1991] here.
</p>

<p>
Suppose \(\psi : \mathbb{R} \to \mathbb{R}\) is continuous, bounded, and
nonconstant. Let \(X \subset \mathbb{R}^N\) be compact. Then a feedforward
neural network with a single hidden layer, all of whose nodes have
activation function \(\psi\), can represent a dense subset of \(C(X)\).
</p>

<p>
Note the conditions on \(\psi\) force it to be nonlinear. Additionally,
the number of nodes in the hidden layer increases rapidly as the
function to be approximated becomes more complex. For the latter reason,
this result's direct applicability is extremely limited; it is useful
mainly as a demonstration that there are no broad classes of
well-behaved functions at which neural network modeling breaks down.
</p>

<p>
The proof of this theorem requires tools from functional analysis such
as the Riesz representation theorem and the Hahn-Banach theorem. While
analogs of these results have been shown for time scales (see [Huseynov
2012],) generalizing the Universal Approximation Theorem to time scales
is beyond the scope of this paper.
</p>
</div>
</div>

<div id="outline-container-orged8a3d7" class="outline-3">
<h3 id="recurrent-neural-networks">Recurrent neural networks</h3>
<div class="outline-text-3" id="text-recurrent-neural-networks">
<p>
This paper's abstract mentioned time-series forecasting among the
applications of neural networks. For computational reasons, the
feedforward architectures we relied on in this paper are generally
insufficient for most applications involving a time domain.
</p>

<p>
<i>Recurrent neural networks</i> were introduced to address these
applications in the late 1980s. They relax the acyclicity of feedforward
neural networks, allowing nodes to feed into themselves or into prior
layers. As a result, applying the total derivative rule to such a node
produces a dynamic equation specialized to the approriate time scale
(differential equations on \(\mathbb{R}\) and difference equations on
\(\mathbb{Z}\).)
</p>

<p>
The present paper applies backpropagation to non-uniform <i>spatial</i>
domains; an interesting application of time scales calculus would be to
generalize backpropagation through time to non-uniform time domains.
</p>
</div>
</div>
</div>

<div id="outline-container-orgcb8c0d6" class="outline-2">
<h2 id="references">References</h2>
<div class="outline-text-2" id="text-references">
<p>
Bohner, Martin, and Gusein Sh. Guseinov. "Partial differentiation on
time scales." <i>Dynamic systems and applications</i> 13, no. 3-4 (2004):
351-379.
</p>

<p>
Bohner, Martin, and Allan Peterson. <i>Dynamic equations on time scales:
an introduction with applications.</i> Springer Science &amp; Business Media, 2012.
</p>

<p>
Ciresan, Dan Claudiu, Ueli Meier, Jonathan Masci, Luca Maria
Gambardella, and Jürgen Schmidhuber. "Flexible, high performance
convolutional neural networks for image classification." In
<i>Twenty-Second International Joint Conference on Artificial
Intelligence</i>. 2011.
</p>

<p>
Hornik, Kurt. "Approximation capabilities of multilayer feedforward
networks." <i>Neural networks</i> 4, no. 2 (1991): 251-257.
</p>

<p>
Huseynov, Adil. "The Riesz representation theorem on time scales."
<i>Mathematical and Computer Modelling</i> 55, no. 3-4 (2012): 1570-1579.
</p>

<p>
Nielsen, Michael A. <i>Neural networks and deep learning.</i> Determination
Press, 2015.
</p>

<p>
Schmidhuber, Jürgen. "Deep learning in neural networks: An overview."
<i>Neural networks</i> 61 (2015): 85-117.
</p>

<p>
Seiffertt, John, and Donald C. Wunsch. "Backpropagation and ordered
derivatives in the time scales calculus." <i>IEEE transactions on neural
networks</i> 21, no. 8 (2010): 1262-1269.
</p>

<p>
Spivak, Michael. <i>Calculus on manifolds: a modern approach to classical
theorems of advanced calculus.</i> W. A. Benjamin, Inc., 1965.
</p>

<p>
Werbos, Paul J. "Backpropagation through time: what it does and how to
do it." <i>Proceedings of the IEEE</i> 78, no. 10 (1990): 1550-1560.
</p>
</article>
</div>
</div>
</div>
</body>
</html>
