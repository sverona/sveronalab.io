<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2020-05-16 Sat 08:57 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>An introduction to the dual simplex method</title>
<meta name="generator" content="Org mode">
<meta name="author" content="Sasha Verona Malone">
<link rel="stylesheet" type="text/css" href="../assets/css/post-style.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2020 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">An introduction to the dual simplex method</h1>
</header><nav id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#the-primal-simplex-method">The primal simplex method</a></li>
<li><a href="#example">Example</a>
<ul>
<li><a href="#iteration-1">Iteration 1</a></li>
<li><a href="#iteration-2">Iteration 2</a></li>
</ul>
</li>
<li><a href="#duality-theory">Duality theory</a></li>
<li><a href="#example-1">Example</a></li>
<li><a href="#motivating-the-dual-simplex-method">Motivating the dual simplex method</a></li>
<li><a href="#applying-the-dual-simplex-method">Applying the dual simplex method</a></li>
<li><a href="#example-2">Example</a>
<ul>
<li><a href="#iteration-1-1">Iteration 1</a></li>
</ul>
</li>
<li><a href="#primal-dual-method">Primal-dual method</a></li>
<li><a href="#references">References</a></li>
</ul>
</div>
</nav>
<article>
<div id="outline-container-org1b09dd0" class="outline-2">
<h2 id="introduction">Introduction</h2>
<div class="outline-text-2" id="text-introduction">
<p>
The primal <i>simplex method</i> provides an algorithmic means for solving
linear programs (hereinafter <i>LPs</i>.) This document motivates, defines,
and applies the so-called <i>dual simplex method</i>, a slight variant which
is useful for performing <i>post hoc</i> sensitivity analysis on LPs solved
by the simplex method.
</p>
</div>
</div>

<div id="outline-container-orgb09715d" class="outline-2">
<h2 id="the-primal-simplex-method">The primal simplex method</h2>
<div class="outline-text-2" id="text-the-primal-simplex-method">
<p>
In this section, we review the three steps of the usual primal method,
following [HL], section 5.2.
</p>

<p>
(This assumes the LP has been augmented by adding slack variables and/or
artificial variables as necessary so that its constraints are in the
form \(\mathbf{Ax} + \mathbf{Ix_s} = \mathbf{b}\).)
</p>

<ol class="org-ol">
<li><i>Initialization</i>: Obtain an initial feasible solution \(\mathbf{x}\).</li>
<li><i>Test for optimality</i>: Check whether there are negative objective
coefficients. If there are none, stop; an optimal solution has been
attained. Otherwise, proceed with step 3.</li>
<li><i>Iteration</i>:

<ol class="org-ol">
<li><i>Determine the entering basic variable.</i> This is done by
identifying the column in which the most negative objective
coefficient occurs.</li>
<li><i>Determine the leaving basic variable.</i> This is done by performing
a <i>minimum ratio test</i>: among the rows with positive entries in
the entering basic variable's column, choose the one whose ratio
of right-hand side to that column's entry is the smallest. The
basic variable in that row is thereby chosen to leave the basis.</li>
<li><i>Perform a change of basis.</i> This is done by performing row
operations to transform the coefficients in the entering basic
variable's column to match the current coefficients in the leaving
basic variable's column.</li>
</ol></li>

<li>Go to step 2.</li>
</ol>
</div>
</div>

<div id="outline-container-org16df2db" class="outline-2">
<h2 id="example">Example</h2>
<div class="outline-text-2" id="text-example">
<p>
Consider the LP
</p>

<p>
\[\begin{aligned} \textrm{maximize} \qquad & z(\mathbf{x}) = 40x_1 +
70x_2 \\ \textrm{subject to} \qquad & x_1 + x_2 \leq 100 \\ & 10x_1 +
50x_2 \leq 4000 \\ \textrm{and} \qquad & x_{1,2} \geq 0. \end{aligned}\]
</p>

<p>
This LP models the following toy problem:
</p>

<blockquote>
<p>
A forestry company has \(100\) acres of timber available for harvest.
There are two options for treating the land after felling the timber
(at a cost of \(10000\) USD per acre); either do nothing (which will
return \(50000\) USD per acre,) or replant the land (which will &#x2014;
eventually &#x2014; return \(120000\) USD per acre, but costs an additional
\(40000\) USD per acre.) The company has \(4000000\) USD available and
wishes to maximize total profit. How many acres of land should be
replanted, and how many should be left alone?
</p>
</blockquote>

<p>
The decision variables \(x_1\) and \(x_2\) represent the quantity of land to
be left alone (replanted.) The first constraint \(x_1 + x_2 \leq 100\)
represents the constraint on available land; the second constraint
\(10x_1 + 50x_2 \leq 4000\) represents the constraint on available capital
(measured in units of \(1000\) USD.)
</p>

<p>
Solving this program by the simplex method proceeds as follows. We first
augment the problem with slack variables \(x_3 = 100 - (x_1 + x_2)\),
\(x_4 = 4000 - (10x_1 + 50x_2)\), giving the form
</p>

<p>
\[\begin{aligned} \textrm{maximize} \qquad & z(\mathbf{x}) = 40x_1 +
70x_2 \\ \textrm{subject to} \qquad & x_1 + x_2 + x_3 = 100 \\ & 10x_1 +
50x_2 + x_4 = 4000 \\ \textrm{and} \qquad & x_{1,2,3,4} \geq 0.
\end{aligned}\]
</p>

<p>
The initial simplex tableau is
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & -40 & -70 & 0 & 0 & 0 \\ x_3 & 1 & 0 &
1 & 1 & 1 & 0 & 100 \\ x_4 & 2 & 0 & 10 & 50 & 0 & 1 & 4000 \\ \hline
\end{array}\]
</p>
</div>

<div id="outline-container-org18cca88" class="outline-3">
<h3 id="iteration-1">Iteration 1</h3>
<div class="outline-text-3" id="text-iteration-1">
<p>
First, we note that the present solution is not optimal (since \(x_1\) and
\(x_2\) have negative coefficients in row 0.)
</p>

<p>
The more negative coefficient is associated with \(x_2\), so we choose it
to enter the basis.
</p>

<p>
Performing a minimum ratio test, we have (for \(x_3\)) \(100 / 1 = 100\) and
(for \(x_4\)) \(4000 / 50 = 80\). Since \(x_4\) has the lower ratio, we choose
it to enter the basis.
</p>

<p>
So we pivot on the \(50\) in \(x_2\)'s column and \(x_4\)'s row:
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & -40 & -70 & 0 & 0 & 0 \\ x_3 & 1 & 0 &
1 & 1 & 1 & 0 & 100 \\ x_4 & 2 & 0 & 10 & \fbox{$50$} & 0 & 1 & 4000\\
\hline \end{array}\]
</p>

<p>
That is to say, we perform row operations to force the coefficients in
\(x_2\)'s column to match the current coefficients in \(x_4\)'s column: -
divide the \(x_4\) row by \(50\), - subtract \(1\) times the resulting row
from the \(x_3\) row, - and subtract \(-70\) times the resulting row from
the \(z\) row.
</p>

<p>
Doing this gives the new tableau
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & -26 & 0 & 0 & 7/5 & 5600 \\ x_3 & 1 & 0
& 4/5 & 0 & 1 & -1/50 & 20 \\ x_2 & 2 & 0 & 1/5 & 1 & 0 & 1/50 & 80\\
\hline \end{array}\]
</p>

<p>
Note that we've updated the basic-variable list in the leftmost column
accordingly.
</p>
</div>
</div>

<div id="outline-container-org2e05b62" class="outline-3">
<h3 id="iteration-2">Iteration 2</h3>
<div class="outline-text-3" id="text-iteration-2">
<p>
The present solution is still not optimal, since there is a \(-26\) in the
objective row.
</p>

<p>
It's in \(x_1\)'s column, so \(x_1\) will enter the basis.
</p>

<p>
Since \(20 / (4/5) = 25 < 400 = 80 / (1/5)\), \(x_3\) will leave the basis.
</p>

<p>
So we'll pivot on the \(4/5\) in \(x_1\)'s column and \(x_3\)'s row. This
gives the tableau
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & 0 & 0 & 75/2 & 3/4 & 6250 \\ x_1 & 1 &
0 & 1 & 0 & 5/4 & -1/40 & 25 \\ x_2 & 2 & 0 & 0 & 1 & -1/4 & 1/40 & 75\\
\hline \end{array}\]
</p>

<p>
This solution is optimal.
</p>
</div>
</div>
</div>

<div id="outline-container-orgc851ab4" class="outline-2">
<h2 id="duality-theory">Duality theory</h2>
<div class="outline-text-2" id="text-duality-theory">
<p>
Recall that to any (primal) LP
\[\begin{aligned}
\textrm{maximize} \qquad& z(\mathbf{x}) = \mathbf{c} \mathbf{x} \\
\textrm{subject to} \qquad& \mathbf{Ax} \leq \mathbf{b}\\
\textrm{and} \qquad& \mathbf{x} \geq \mathbf{0}
\end{aligned} \]
we may associate a <i>dual</i> LP
\[\begin{aligned}
\textrm{minimize} \qquad& w(\mathbf{y}) = \mathbf{y} \mathbf{b} \\
\textrm{subject to} \qquad& \mathbf{yA} \geq \mathbf{c}\\
\textrm{and} \qquad& \mathbf{y} \geq \mathbf{0}
\end{aligned}\]
(where \(\mathbf{y}\) is a row vector,) or, equivalently,
\[\begin{aligned}
\textrm{minimize} \qquad& w(\mathbf{y}) = \mathbf{b}^\top\mathbf{y} \\
\textrm{subject to} \qquad& \mathbf{A}^\top\mathbf{y} \geq \mathbf{c}^\top\\
\textrm{and} \qquad& \mathbf{y} \geq \mathbf{0}
\end{aligned}\] (where \(\mathbf{y}\) is a column vector.)
</p>
</div>
</div>

<div id="outline-container-org973d4e2" class="outline-2">
<h2 id="example-1">Example</h2>
<div class="outline-text-2" id="text-example-1">
<p>
The foregoing (primal) LP has dual
</p>

<p>
\[\begin{aligned} \textrm{minimize} \qquad & w(\mathbf{y}) = 100y_1 +
4000y_2 \\ \textrm{subject to} \qquad & y_1 + 10y_2 \geq 40 \\ & y_1 +
50y_2 \geq 70 \\ \textrm{and} \qquad & y_{1,2} \geq 0. \end{aligned}\]
</p>

<p>
This LP may be thought of as modeling the problem of finding an <i>upper
bound</i> for the primal LP as a linear combination of constraints, where
the variables \(y_i\) represent the multiplier of the \(i\)th constraint.
</p>
</div>
</div>

<div id="outline-container-org8e2f160" class="outline-2">
<h2 id="motivating-the-dual-simplex-method">Motivating the dual simplex method</h2>
<div class="outline-text-2" id="text-motivating-the-dual-simplex-method">
<p>
An important property of the primal simplex method is that the solution
attained at each iteration is (primal) feasible.<label for='98452141' class='margin-toggle sidenote-number'></label><input id='98452141' type='checkbox' class='margin-toggle' /><span class='sidenote'>This is
assured inductively by the minimum ratio test.</span></label> Similarly, the dual
simplex method attains a <i>dual</i>-feasible solution at each iteration.
</p>

<p>
Therefore, if perturbing a primal problem causes the new final solution
to be dual-feasible but not primal-feasible, the dual simplex method may
be used to attain the new optimal solution.
</p>

<p>
An important point to note is that the dual simplex method is, in a
sense, the dual <i>algorithm</i> of the ordinary simplex method. It is not
the same as just applying the ordinary simplex method to the dual,
although it performs almost all the same calculations. In particular, it
operates on the primal tableau rather than the dual tableau.
</p>
</div>
</div>

<div id="outline-container-org7aa4d8b" class="outline-2">
<h2 id="applying-the-dual-simplex-method">Applying the dual simplex method</h2>
<div class="outline-text-2" id="text-applying-the-dual-simplex-method">
<ol class="org-ol">
<li><i>Initialization</i>: Obtain an initial dual-feasible solution
\(\mathbf{y}\). In practice, this will be given by a final primal
tableau.</li>
<li><i>Test for feasibility</i>: Check whether there are negative right-hand
sides. If there are none, stop; an primal-feasible (and hence
primal-optimal) solution has been attained. Otherwise, proceed with
step 3.</li>
<li><i>Iteration</i>:

<ol class="org-ol">
<li><i>Determine the leaving basic variable.</i> This has already been done
by identifying the row in which the negative right-hand side
occurs.</li>
<li><i>Determine the entering basic variable.</i> Among those columns in
the pivot row whose coefficient is negative, choose the one whose
ratio of that coefficient to its corresponding objective
coefficient is minimized (in absolute value.)</li>
<li><i>Perform a change of basis.</i> This is done as before.</li>
</ol></li>

<li>Go to step 2.</li>
</ol>

<p>
Note that the variables to be exchanged in the basis are determined in
the opposite order of the primal simplex method. This reflects the fact
that (after augmenting both problems) any variable that is basic in the
primal corresponds to a nonbasic variable in the dual, and vice versa.
</p>
</div>
</div>

<div id="outline-container-orgec505dd" class="outline-2">
<h2 id="example-2">Example</h2>
<div class="outline-text-2" id="text-example-2">
<p>
Suppose the forestry company decides to sell 25 acres of its land at the
price of \(6000\) USD an acre. Then the right-hand sides in the primal LP
(ref) are changed, giving the problem
</p>

<p>
\[\begin{aligned} \textrm{maximize} \qquad & z(\mathbf{x}) = 40x_1 +
70x_2 \\ \textrm{subject to} \qquad & x_1 + x_2 \leq 75 \\ & 10x_1 + 50x_2
\leq 4150 \\ \textrm{and} \qquad & x_{1,2} \geq 0. \end{aligned}\]
</p>

<p>
That is, there are 25 fewer acres of land and \(250,\!000\) USD more than
in the original setup.
</p>

<p>
The optimal solution we found in the original problem \((25, 75)\) is not
primal-feasible in this new problem, but it is dual-feasible, as may be
checked.
</p>

<p>
We would like to find the new optimal solution for this perturbed
problem without effectively retracing the steps we took to solve the
original problem.
</p>

<p>
So we start with the final tableau from the original problem, which was
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & 0 & 0 & 75/2 & 3/4 & 6250 \\ x_1 & 1 &
0 & 1 & 0 & 5/4 & -1/40 & 25 \\ x_2 & 2 & 0 & 0 & 1 & -1/4 & 1/40 & 75\\
\hline \end{array}\]
</p>

<p>
This was arrived at after a series of row operations, the net effect of
which was to transform the coefficients of the slack variables \(x_3\) and
\(x_4\) from the identity matrix to
</p>

<p>
\[\mathbf{B}^{-1} = \begin{bmatrix} 5/4 & -1/40 \\ -1/4 & 1/40 \end{bmatrix}.\]
</p>

<p>
As can be verified, the entire rows 1 and 2 of the final tableau can be
obtained by simply premultiplying the initial rows 1 and 2 by this
matrix.
</p>

<p>
Suppose we started instead with the appropriate initial tableau for the
new problem, with modified right sides, and performed all the same row
operations. Then &#x2014; multiplying through by \(\mathbf{B}^{-1}\) &#x2014; we
find that the variable coefficients will be the same, and the new
right-hand sides will be given by
</p>

<p>
\[\begin{bmatrix} 5/4 & -1/40 \\ -1/4 & 1/40 \end{bmatrix}\begin{bmatrix} 75 \\ 4150 \end{bmatrix} = \begin{bmatrix} -10 \\ 85\end{bmatrix} .\]
</p>

<p>
So the <i>initial</i> tableau for the dual simplex method will be
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & 0 & 0 & 75/2 & 3/4 & 5550 \\ x_1 & 1 &
0 & 1 & 0 & 5/4 & -1/40 & -10 \\ x_2 & 2 & 0 & 0 & 1 & -1/4 & 1/40 & 85 \\
\hline \end{array}\]
</p>
</div>

<div id="outline-container-org2b8c23e" class="outline-3">
<h3 id="iteration-1-1">Iteration 1</h3>
<div class="outline-text-3" id="text-iteration-1-1">
<p>
We observe that the solution is not primal-feasible, since there is a
\(-10\) in the right-hand side. Since this is in \(x_1\)'s row, \(x_1\) will
leave the basis.
</p>

<p>
To select the entering basic variable, we examine the variables in
\(x_1\)'s row with negative coefficients. There is only one, the \(-1/40\)
in \(x_4\)'s column, so \(x_4\) will enter the basis.<label for='4e44ff24' class='margin-toggle sidenote-number'></label><input id='4e44ff24' type='checkbox' class='margin-toggle' /><span class='sidenote'>Were there
more than one, we would compare \(\vert(3/4)/(-1/40)\vert\) to the
corresponding ratios in the other columns, and choose the variable with
the lowest ratio.</span></label>
</p>

<p>
Thus we pivot on that \(-1/40\): \[\begin{array}{|c|c|rrrrr|r|} \hline
\textbf{BV} & \textbf{Eq'n} & \mathbf{z} & \mathbf{x_1} & \mathbf{x_2} &
\mathbf{x_3} & \mathbf{x_4} & \textbf{RHS} \\ \hline z & 0 & 1 & 0 & 0 &
75/2 & 3/4 & 5550 \\ x_1 & 1 & 0 & 1 & 0 & 5/4 & \fbox{$-1/40$} & -10\\
x_2 & 2 & 0 & 0 & 1 & -1/4 & 1/40 & 85 \\ \hline \end{array}\]
</p>

<p>
This gives
</p>

<p>
\[\begin{array}{|c|c|rrrrr|r|} \hline \textbf{BV} & \textbf{Eq'n} &
\mathbf{z} & \mathbf{x_1} & \mathbf{x_2} & \mathbf{x_3} & \mathbf{x_4} &
\textbf{RHS} \\ \hline z & 0 & 1 & 30 & 0 & 0 & 0 & 5250 \\ x_4 & 1 & 0 &
-40 & 0 & -50 & 1 & 400 \\ x_2 & 2 & 0 & 1 & 1 & 1 & 0 & 75 \\ \hline
\end{array}\]
</p>

<p>
Since there are no longer negative right-hand sides, the present
solution \((0, 75)\) is primal-feasible and hence optimal.
</p>
</div>
</div>
</div>

<div id="outline-container-org687ac33" class="outline-2">
<h2 id="primal-dual-method">Primal-dual method</h2>
<div class="outline-text-2" id="text-primal-dual-method">
<p>
The scenarios in which we can immediately apply the dual simplex method
are limited to those in which the starting point is dual-feasible and
primal-infeasible.
</p>

<p>
Were the starting point instead primal-feasible and dual-infeasible, we
could simply apply the primal simplex method.
</p>

<p>
The remaining case &#x2013; points that are infeasible both in the primal and
the dual &#x2013; is a little more involved. We follow [Va], &sect; 6.5.
</p>

<p>
We know a solution is primal-feasible (dual-feasible) only if it has no
negative right-hand sides (no negative objective coefficients); so there
must be at least one negative entry in each.
</p>

<p>
The idea is to solve an <i>intermediate problem</i> given by replacing some
of the offending coefficients with zeros. (This can be done in either
the objective row or the right-hand side column, but not both.) This can
be done by the primal or dual simplex method, depending on whether the
initial intermediate tableau is primal- or dual-feasible.
</p>

<p>
Once this has been done, the previously zeroed-out negative coefficients
can be restored, which gives an initial feasible solution for the real
problem. This can be solved using the variant of the simplex method that
was not used to solve the intermediate problem.
</p>

<p>
If the intermediate was solved with the dual simplex method, then it
must have had its negative objective coefficients zeroed out, and
restoring these will give a primal-feasible initial solution, suitable
for initializing the primal simplex method. Similarly, if the
intermediate was solved with the primal simplex method, the real problem
will require the dual simplex method.
</p>
</div>
</div>

<div id="outline-container-org5900447" class="outline-2">
<h2 id="references">References</h2>
<div class="outline-text-2" id="text-references">
<p>
[Va] Vanderbei [HL] Hillier/Lieberman
</p>

</article>
</div>
</div>
</div>
</body>
</html>
