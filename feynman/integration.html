<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2020-05-16 Sat 08:57 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Feynman Mathematical Methods: Integration</title>
<meta name="generator" content="Org mode">
<meta name="author" content="James Keck, transcribed by SVM">
<link rel="stylesheet" type="text/css" href="../assets/css/post-style.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2020 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">Feynman Mathematical Methods: Integration</h1>
</header><nav id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgcbfb648">Methods</a></li>
<li><a href="#org6cebbcf">Complex Variable in Substitution</a></li>
<li><a href="#org72d532b">Differentiation of a Parameter</a></li>
<li><a href="#org5b73164">Multiplication by a Factor</a></li>
<li><a href="#orgceaaa66">Differentiation under an Integral Sign</a>
<ul>
<li><a href="#org8dc3204">Partial Differentiation</a></li>
</ul>
</li>
<li><a href="#org4bf6cff">The Dirac Delta Function</a>
<ul>
<li><a href="#org77dd12d">Example: Use of the delta function in evaluating a definite integral</a></li>
</ul>
</li>
<li><a href="#orgb9cbf25">Investigation of the Existence of Integrals</a></li>
<li><a href="#org34bf0c4">Special Method of Evaluating a Definite Integral</a></li>
<li><a href="#org2d824f1">Series Evaluation of Definite Integrals<label for='f62f7c5d' class='margin-toggle sidenote-number'></label><input id='f62f7c5d' type='checkbox' class='margin-toggle' /><span class='sidenote'>This section was originally titled "Series Solution of Differential Equations," but that doesn't fit.</span></label></a></li>
<li><a href="#orga0ac3da">Problems</a></li>
</ul>
</div>
</nav>
<article>

<p>
Integration
</p>
<div id="outline-container-orgcbfb648" class="outline-2">
<h2 id="orgcbfb648">Methods</h2>
<div class="outline-text-2" id="text-orgcbfb648">
<ol class="org-ol">
<li>Substitution</li>
<li>By parts</li>
<li>Differentiation of a parameter</li>
<li>Series expansion</li>
<li>Contour integration</li>
<li>Numerical methods</li>
<li>Special tricks</li>
</ol>

<p>
Methods 3 to 7 are applicable particularly to definite integrals.
</p>
</div>
</div>

<div id="outline-container-org6cebbcf" class="outline-2">
<h2 id="org6cebbcf">Complex Variable in Substitution</h2>
<div class="outline-text-2" id="text-org6cebbcf">
<p>
Find \(\int_0^\infty e^{-ax} \cos bx ~dx.\)
</p>

<p>
Since \(\cos bx = (e^{ibx} + e^{-ibx})/2\), we have
</p>

<p>
\[\begin{aligned}
\int_0^\infty e^{-ax} \cos bx &= \frac12 \int_0^\infty e^{-(a - ib)x} ~dx + \frac12 \int_0^\infty e^{-(a + ib)x} ~dx \\
&= \frac12 \left( \frac1{a - ib} + \frac1{a + ib} \right) \\
&= \frac{a}{a^2 + b^2}
\end{aligned}\]
</p>
</div>
</div>

<div id="outline-container-org72d532b" class="outline-2">
<h2 id="org72d532b">Differentiation of a Parameter</h2>
<div class="outline-text-2" id="text-org72d532b">
<p>
Find \(\displaystyle \int_0^\infty xe^{-ax} \cos bx ~dx.\)
</p>

<p>
Define \(\displaystyle S(a) = \int_0^\infty e^{-ax} \cos bx ~dx = \frac{a}{a^2 + b^2}.\)
</p>

<p>
Differentiate with respect to \(a\):
</p>

<p>
\[
S'(a) = -\int_0^\infty xe^{-ax} \cos bx = \frac{b^2 - a^2}{(a^2 + b^2)^2}
\]
</p>

<p>
The general rule for differentiation with respect to a
parameter is
</p>

<p>
\(\displaystyle \frac{d}{d\alpha} \left[ \int_{x_1(\alpha)}^{x_2(\alpha)} f(x, \alpha) ~dx\right] = \int_{x_1}^{x_2} \frac{\partial}{\partial\alpha} f(x, \alpha) ~dx + \left[\frac{d}{d\alpha} x_2(\alpha)\right]\cdot f(x_2, \alpha) - \left[\frac{d}{d\alpha} x_1(\alpha)\right]\cdot f(x_1, \alpha)\)
</p>


<figure>
<img src="figs/diffunderintplot.png" alt="diffunderintplot.png" class="fullwidth">

</figure>



<p>
\[\begin{aligned}
\Delta \int_{x_1}^{x_2} f(x, \alpha) ~dx &= \int_{x_1}^{x_2} \Delta f(x, \alpha) ~dx \\
&+ \Delta x_2(\alpha) f(x_2, \alpha) - \Delta x_1(\alpha) f(x, \alpha)
\end{aligned}
\]
</p>
</div>
</div>

<div id="outline-container-org5b73164" class="outline-2">
<h2 id="org5b73164">Multiplication by a Factor</h2>
<div class="outline-text-2" id="text-org5b73164">
<p>
Find \(\displaystyle \int_0^\infty \frac{\sin x}x ~dx.\)
</p>

<p>
\[\begin{aligned}
S(\alpha) &= \int_0^\infty e^{-ax} \frac{\sin x}x ~dx \\
S'(\alpha) &= -\int_0^\infty e^{-ax} \sin x ~dx = \frac{-1}{1 + \alpha^2} \\
S(\alpha) &= -\arctan \alpha + C \\
\lim_{\alpha \to \infty} S(\alpha) &= 0 = -\frac\pi{2} + C \\
S(\alpha) &= \frac\pi{2} - \arctan \alpha \\
S(0) &= \frac\pi{2}
\end{aligned}\]
</p>
</div>
</div>

<div id="outline-container-orgceaaa66" class="outline-2">
<h2 id="orgceaaa66">Differentiation under an Integral Sign</h2>
<div class="outline-text-2" id="text-orgceaaa66">
<p>
\[\begin{aligned}
\int_0^\infty e^{-ax} (1 + x^2)^{-1} ~dx &= S(a) \\
\int_0^\infty x^2 e^{-ax} (1 + x^2)^{-1} ~dx &= S''(a)
\end{aligned}\]
</p>

<p>
\[\begin{aligned}
S(a) + S''(a) &= \int_0^\infty \left(\frac1{1 + x^2} + \frac{x^2}{1 + x^2}\right) e^{-ax} ~dx \\
\frac{d^2S}{da^2} + S &= \frac1a
\end{aligned}\]
</p>

<p>
This differential equation may be solved for \(S(a)\).
</p>
</div>

<div id="outline-container-org8dc3204" class="outline-3">
<h3 id="org8dc3204">Partial Differentiation</h3>
<div class="outline-text-3" id="text-org8dc3204">
<p>
\[\begin{aligned}
S(\alpha, \beta) &= \int_0^\infty e^{-\alpha x} \sin \beta x ~dx \\
\frac{\partial^2 S}{\partial \beta^2} &= -\int_0^\infty x^2 e^{-\alpha x} \sin \beta x ~dx \\
\frac{\partial^2 S}{\partial \alpha^2} &= \int_0^\infty x^2 e^{-\alpha x} \sin \beta x ~dx \\
\frac{\partial^2 S}{\partial \beta^2} &= - \frac{\partial^2 S}{\partial \alpha^2}
\end{aligned}\]
</p>

<p>
The form of \(S\) may be partly determined by making the substitution \(y = \beta x\).
</p>

<p>
\[\begin{aligned}
S(\alpha, \beta) &= \frac1\beta \int_0^\infty e^{-\frac\alpha\beta y} \sin y ~dy = \frac1\beta F\left(\frac\alpha\beta\right)\\
\frac\partial{\partial\beta} \left[ \frac1\beta F\left(\alpha\beta\right) \right] &= \frac1\beta F'\left(-\frac\alpha{\beta^2}\right) - \frac{F\left(\frac\alpha\beta\right)}{\beta^2} \\
\frac{\partial^2}{\partial\beta^2}\left[\frac1\beta F\left(\alpha\beta\right)\right] &= -\frac{\alpha}{\beta^3} F''\left(-\frac{\alpha}{\beta^2}\right) + F'\left(\frac\alpha\beta\right)\frac{3\alpha}{\beta^4} \\
&+ F\left(\frac\alpha\beta\right) \frac2{\beta^3} - \frac1{\beta^2}F'\left(-\frac{\alpha}{\beta^2}\right) \\
\\
\frac{\partial^2S}{\partial\beta^2} &= \frac2{\beta^3}F + \frac{4\alpha}{\beta^4}F' + \frac{\alpha^2}{\beta^5}F'' \\
\frac{\partial^2S}{\partial\alpha^2} &= \frac1{\beta^3}F''
\end{aligned}\]
</p>

<p>
Multiply through by \(\beta^3\) and let \(z = \frac{\alpha}{\beta}\).<label for='10754add' class='margin-toggle sidenote-number'></label><input id='10754add' type='checkbox' class='margin-toggle' /><span class='sidenote'>The original has \(\frac{d}{dx}\) for \(\frac{d}{dz}\) in what follows.</span></label>
</p>

<p>
\[\begin{aligned}
2F + 4zF' + z^2F'' &= -F'' \\
\frac{d}{dz}(z^2F') + 2\frac{d}{dz}(zF) &= -F'' \\
z^2F' + 2zF &= -F' + C_1 \\
\frac{d}{dz}(z^2F) &= -F' + C_1 \\
z^2F &= -F + C_1z + C_0 \\
\end{aligned}\]
</p>

<p>
\[\begin{aligned}
F &= \frac{C_0 + C_1z}{1 + z^2}\\
S(\alpha, \beta) &= \frac1\beta F = \frac{C_0\beta + C_1\alpha}{\alpha^2 + \beta^2}
\end{aligned}\]
</p>

<p>
To evaluate \(c_0\) and \(c_1\), we observe:
</p>

<p>
\[
S(\alpha, 0) = 0 = \frac{C_1\alpha}{\alpha_2} \quad\therefore C_1 = 0
\]
</p>

<p>
For small \(\beta\), \(\sin \beta x \sim \beta x\). This is a good approximation since \(e^{-\alpha x}\) kills the integrand for large \(x\).
</p>

<p>
\[\begin{aligned}
\int_0^\infty e^{-\alpha x} \beta x ~dx &= \frac\beta{\alpha^2} \\
\frac{C_0\beta}{\alpha^2 + \beta^2} &\cong \frac{C_0\beta}{\alpha^2} \quad\therefore C_0 = 1
\end{aligned}\]
</p>

<p>
\[
S(\alpha, \beta) = \frac\beta{\alpha^2 + \beta^2}
\]
</p>
</div>
</div>
</div>

<div id="outline-container-org4bf6cff" class="outline-2">
<h2 id="org4bf6cff">The Dirac Delta Function</h2>
<div class="outline-text-2" id="text-org4bf6cff">
<p>
\(\delta(x) = 0\), \(x =\not 0\), and is in some way infinite for \(x = 0\).<label for='454b1efe' class='margin-toggle sidenote-number'></label><input id='454b1efe' type='checkbox' class='margin-toggle' /><span class='sidenote'>The reference for this section is <i>The Principles of Quantum Mechanics</i>, P. M. Dirac.</span></label>
</p>

<p>
\[\begin{gathered}
\int_{-\infty}^\infty \delta(x) ~dx \vcentcolon= \int_{-\epsilon}^{\epsilon} \delta(x) ~dx \vcentcolon= 1 \\
\int_{-\infty}^\infty f(x) \delta(x) ~dx = f(0)
\end{gathered}\]
</p>

<p>
Proof:
</p>

<p>
Write \(\displaystyle
\int_{-\infty}^\infty f(x) \delta(x) ~dx = \int_{-\infty}^{-\epsilon} f(x) \delta(x) ~dx + \int_{-\epsilon}^\epsilon f(x) \delta(x) ~dx + \int_\epsilon^\infty f(x) \delta(x) ~dx. \)
</p>

<p>
If \(f(x)\) varies relatively slowly in the range \([-\epsilon, \epsilon]\), we can replace it by an average of \(f(x)\) over that interval.
</p>

<p>
\[\begin{aligned}
\int_{-\infty}^\infty f(x) \delta(x) ~dx &= \int_{-\epsilon}^\epsilon f(x) \delta(x) ~dx \\
&= \int_{-\epsilon}^\epsilon \overline{f(x)} \delta(x) ~dx \\
&= \overline{f(x)} \cdot \mathbf{1}_{x = 0} \cdot \int_{-\epsilon}^\epsilon \delta(x) ~dx \\
\left(\lim_{\epsilon \to 0}\right) \quad &= f(0).
\end{aligned}\]
</p>

<p>
The delta function is useful since it may be operated on as though it were a real function. The only justification we give for this is that it gives the correct answer.
</p>

<p>
\[\begin{aligned}
f'(t) &= -\int_{-\infty}^\infty f(x) \delta'(x - t) ~dx \\
&= \left. f(x) \delta(x - t) \right\vert_{x=-\infty}^{x=\infty} - \int_{-\infty}^\infty f'(x) \delta(x - t) ~dx \\
&= f'(t).
\end{aligned}\]
</p>

<p>
The following useful relations may be easily proved:
</p>

<ol class="org-ol">
<li>\(\delta(x) = \delta(-x)\)</li>
<li>\(\delta'(x) = -\delta'(-x)\)</li>
<li>\(x \delta(x) \equiv 0 \)</li>
<li>\(x\delta'(x) = -\delta(x)\)</li>
<li>\(\delta(ax) = \frac1{\vert a\vert} \delta(x)\)</li>
<li>\(f(x)\delta(x - a) = f(a)\delta(x - a)\)</li>
</ol>


<figure>
<img src="figs/diracdeltaplot.png" alt="diracdeltaplot.png">

<figcaption><span class="figure-number">Figure 2: </span>Approximate representation of \(\delta(x)\) (actually \(e^{-400x^2}\))</figcaption>
</figure>


<figure>
<img src="figs/diracdeltaprimeplot.png" alt="diracdeltaprimeplot.png">

<figcaption><span class="figure-number">Figure 3: </span>Approximate representation of \(\delta'(x)\) (actually \(-800xe^{-400x^2}\))</figcaption>
</figure>

<p>
The following figures are approximate representations of \(\delta(x)\) and \(\delta'(x)\).
</p>

<p>
Define the <i>step function</i> \(\displaystyle H(x) = \int_{-\infty}^x \delta(x) ~dx = \begin{cases} 0, &x < 0,\\ 1, &x >0.\end{cases}\)<label for='80b6e287' class='margin-toggle sidenote-number'></label><input id='80b6e287' type='checkbox' class='margin-toggle' /><span class='sidenote'>There are differing definitions of the value at \(x = 0\). The definition of \(\delta(x)\) as the limit \(\sigma \to 0\) of the normal distribution suggests the value should be \(1/2\), but some authors define it as \(1\).</span></label>
</p>


<figure>
<img src="figs/piecewisesmooth.png" alt="piecewisesmooth.png">

<figcaption><span class="figure-number">Figure 4: </span>Example of a curve whose derivative may be expressed using \(\delta(x)\)</figcaption>
</figure>

<p>
Consider the<label for='6285c483' class='margin-toggle sidenote-number'></label><input id='6285c483' type='checkbox' class='margin-toggle' /><span class='sidenote'>piecewise smooth</span></label> curve above. Its derivative may be expressed in terms of the delta function as \(f'(x)_{x \not= a} + c\delta(x - a)\).
</p>

<p>
Consider the integral \(\int_{-\infty}^\infty \cos \beta x ~dx\). We shall show that it is equal to \(2\pi \delta(\beta)\) by showing
\[\int_0^\infty \cos \beta x ~dx = \pi \delta(\beta). \]
</p>

<p>
Note \(\displaystyle\lim_{\alpha \to 0} e^{-\alpha x} \cos \beta x ~dx = \lim_{\alpha \to 0} \frac\alpha{\alpha^2 + \beta^2}.\)<label for='23465fae' class='margin-toggle sidenote-number'></label><input id='23465fae' type='checkbox' class='margin-toggle' /><span class='sidenote'>You may argue here that we haven't shown that this limit is actually equal to some multiple of \(\delta(\beta)\). You'd be correct to do so. The fact Feynman is implicitly using here is that the Dirac delta is the limit of a Cauchy distribution as the scale parameter goes to \(0\). In fact, Cauchy defined the delta this way 100 years before Dirac!</span></label>
</p>


<figure>
<img src="figs/cauchydist.png" alt="cauchydist.png">

<figcaption><span class="figure-number">Figure 5: </span>The Cauchy distribution for small \(\gamma\).</figcaption>
</figure>

<p>
For small \(\alpha\) this integral looks approximately as above. The area under the curve is \(A = \int_{-\infty}^\infty \frac\alpha{\alpha^2+\beta^2} ~d\beta.\)
</p>

<p>
Making the change of variables \(\beta = \alpha z\), we obtain
</p>

<p>
\[\begin{aligned}
A &= \int_{-\infty}^\infty \frac{\alpha^2}{\alpha^2 + \alpha^2 z^2} ~dz \\
&= \int_{-\infty}^\infty \frac{dz}{1 + z^2} \\
&= \arctan z \vert_{-\infty}^\infty \\
&= \pi.
\end{aligned}\]
</p>

<p>
Note that the area is independent of the value of \(\alpha\).<label for='d1fae2a3' class='margin-toggle sidenote-number'></label><input id='d1fae2a3' type='checkbox' class='margin-toggle' /><span class='sidenote'>And thus \(A\) is equal to the multiple of \(\delta(\beta)\) whose integral over the real line is \(\pi\), which is \(\pi \delta(\beta)\).</span></label>
</p>
</div>

<div id="outline-container-org77dd12d" class="outline-3">
<h3 id="org77dd12d">Example: Use of the delta function in evaluating a definite integral</h3>
<div class="outline-text-3" id="text-org77dd12d">
<p>
\[\int_0^{\pi/2} \cos(m \tan\theta) ~d\theta \]
</p>

<p>
Substitute \(\tan \theta = x\).
</p>

<p>
\[\begin{aligned}
\int_0^\infty \cos mx ~\frac{dx}{1 + x^2} &= S(m) \\
-\int_0^\infty x^2 \cos mx ~\frac{dx}{1 + x^2} &= S''(m) \\
\end{aligned}\]
</p>

<p>
\[S(m) - S''(m) = \int_0^\infty \cos mx ~dx = \pi \delta(m)\]
</p>

<div class="solution-wrapper">
<label class="show-solution" for="variation-of-parameters">Aside: Variation of Parameters</label>
<input type="checkbox" class="show-solution" id="variation-of-parameters"/>
<div class="solution" style="font-size: 0.8rem; line-height: 150%;">
<p>
This is a perfectly general method for solving inhomogeneous differential equations. It shows up in any undergraduate course, but this example begs for more details here, so let's go through it now.
</p>

<p>
In fact, let's solve the more general equation \(S''(m) - S(m) = f(m)\). First, we find the homogeneous solutions \(h_1(m) = e^m\), \(h_2(m) = e^{-m}\). These are so named because they solve the <i>homogeneous equation</i> \(S''(m) - S(m) = 0\).
</p>

<p>
Now let's assume the solution is given by<label for='2e98fb42' class='margin-toggle sidenote-number'></label><input id='2e98fb42' type='checkbox' class='margin-toggle' /><span class='sidenote'>This is why the method is called <i>variation of parameters</i>; the "parameters" are the "coefficients" on \(h_1\) and \(h_2\), which would be constants were this equation homogeneous; but we've just made them variables.</span></label>
\[S(m) = s_1(m)h_1(m) + s_2(m)h_2(m) \]
and further assume<label for='c84dee6f' class='margin-toggle sidenote-number'></label><input id='c84dee6f' type='checkbox' class='margin-toggle' /><span class='sidenote'>Yes, there is a valid reason we can make all these assumptions; it has to do with something called <i>differential algebra</i>, which explains how the derivative acts as a linear operator on a function space. An article about this is forthcoming. Here, though, it's beside the point.</span></label>
\[0 = s_1'(m)h_1(m) + s_2'(m)h_2(m). \]
</p>

<p>
If we differentiate the foregoing equation, we get
\[\begin{aligned}
S'(m) &= s_1'(m)h_1(m) + s_1(m)h_1'(m) \\
&\quad + s_2'(m)h_2(m) + s_2(m)h_2'(m) \\
&=s_1(m)h_1'(m) + s_2(m)h_2'(m).
\end{aligned}\]
</p>

<p>
If we do it again, we get
</p>

<p>
\[\begin{aligned}
S''(m) &= s_1'(m) h_1'(m) + s_1(m) h_1''(m) \\
&\quad+ s_2'(m) h_2'(m) + s_2(m) h_2''(m).
\end{aligned}\]
</p>

<p>
Now we have
\[\begin{aligned}
f(m) = S''(m) - S(m) &= s_1'(m)h_1'(m) + s_1(m)h_1''(m) \\
&\quad + s_2'(m)h_2'(m) + s_2(m)h_2''(m) \\
&\quad - s_1(m)h_1(m) - s_2(m)h_2(m) \\
&= s_1'(m)h_1'(m) + s_2'(m)h_2'(m).
\end{aligned}\]
</p>

<p>
Thus we obtain the system of equations<label for='bd01a7fb' class='margin-toggle sidenote-number'></label><input id='bd01a7fb' type='checkbox' class='margin-toggle' /><span class='sidenote'>This trick of "constructing \(f(m)\)" is the key step here; it goes through regardless of what the original equation was. Everything after this is problem-specific.</span></label>
</p>

<p>
\[\begin{bmatrix} h_1(m) & h_2(m) \\ h_1'(m) & h_2'(m) \end{bmatrix}
\begin{bmatrix} s_1'(m) \\ s_2'(m) \end{bmatrix} =
\begin{bmatrix} 0 \\ f(m)\end{bmatrix}\]
</p>

<p>
In our case, this becomes
</p>

<p>
\[\begin{aligned} e^m s_1'(m) + e^{-m} s_2'(m) &= 0, \\
e^ms_1'(m) - e^{-m} s_2'(m) &= -\pi\delta(m),\end{aligned}
\]
</p>

<p>
which gives \(2e^m s_1'(m) = -\pi \delta(m)\), \(2e^{-m}s_2'(m) = \pi\delta(m)\), so we have
</p>

<p>
\[\begin{aligned} s_1(m) &= -\frac\pi2 \int e^{-m} \delta(m) ~dm \\
&= -\frac\pi2 e^{-0}H(m) + A, \\
s_2(m) &= \frac\pi2 \int e^m \delta(m) ~dm \\
&= \frac\pi2 e^0H(m) + B.
\end{aligned}\]
</p>

<p>
Thus the solution is given by
</p>

<p>
\[\begin{aligned}
S(m) &= \left(-\frac\pi2 H(m) + A\right) e^m + \left(\frac\pi2 H(m) + B\right)e^{-m} \\
&= \frac\pi2 H(m) (-e^m + e^{-m}) + Ae^m + Be^{-m},
\end{aligned}\]
</p>

<p>
which agrees with what is given below.
</p>
</div>
</div>

<p>
The general solution of such a differential equation is<label for='4feb0366' class='margin-toggle sidenote-number'></label><input id='4feb0366' type='checkbox' class='margin-toggle' /><span class='sidenote'>Obtained by variation of parameters.</span></label>
</p>

<p>
\(\displaystyle S(m) = \frac{e^m}2 \int_{-\infty}^m e^{-t} f(t) ~dt - \frac{e^{-m}}2\int_{-\infty}^m e^t f(t) ~dt + Ae^m + Be^{-m}\)
</p>

<p>
where \(f(t) = \pi \delta(t)\).
</p>

<p>
Therefore the solution has the form
</p>

<p>
\[S(m) = \begin{cases}Ae^m + Be^{-m}, &m < 0, \\ Ae^m + Be^{-m} - \frac\pi2 e^m + \frac\pi2 e^{-m}, &m > 0,\end{cases}\]
</p>

<p>
where the constants \(A\), \(B\) still have to be determined. Note \(S(0) = \pi/2 = A + B\). Also note that \(S(m)\) is an even function<label for='5a71bb46' class='margin-toggle sidenote-number'></label><input id='5a71bb46' type='checkbox' class='margin-toggle' /><span class='sidenote'>Note that \(T(\theta) = \int \cos(m \tan \theta) ~dm\) would be odd.</span></label>, so we have<label for='6e0c0948' class='margin-toggle sidenote-number'></label><input id='6e0c0948' type='checkbox' class='margin-toggle' /><span class='sidenote'>After assuming without loss of generality that \(m > 0\).</span></label>
</p>

<p>
\[\begin{aligned}
S(-m) &= S(m) \\
Ae^{-m} + Be^m &= Ae^m + Be^{-m} - \frac\pi2 e^m + \frac\pi2 e^{-m} \\
(B - A)(e^{-m} - e^m) &= -\frac\pi2 (e^{-m} - e^m) \\
B - A &= -\frac\pi2,
\end{aligned}\]
</p>

<p>
from which it follows that \(A = \pi/2\) and \(B = 0\), and we finally obtain
</p>

<p>
\[\begin{aligned}
S(m) &= \begin{cases} \frac\pi2 e^m, &m < 0, \\ \frac\pi2 e^{-m}, &m \geq 0.\end{cases}
\end{aligned}\]
</p>
</div>
</div>
</div>

<div id="outline-container-orgb9cbf25" class="outline-2">
<h2 id="orgb9cbf25">Investigation of the Existence of Integrals</h2>
<div class="outline-text-2" id="text-orgb9cbf25">
<p>
Consider \(\displaystyle \int_0^\infty \frac{e^{-ax}}x ~dx.\)
</p>

<p>
Near \(0\), this integral is approximately \(\int_0^\infty \frac1x ~dx = -\ln 0 + \dots\). The integral is infinite.
</p>

<p>
Consider \(\displaystyle \int_0^\infty \frac{1 - e^{-ax}}x ~dx.\)
</p>

<p>
As \(x \to 0\), the integral is approximately \(\int_0^\infty a ~dx = 0 + \dots\).
</p>

<p>
As \(x \to \infty\), the integral is approximately \(\int_0^\infty \frac1x ~dx = \ln\infty + \dots\). Again, the integral is infinite.
</p>

<p>
The following table of functions may prove useful in determining whether or not a given integral exists.
</p>

<table>
<caption class="t-above"><span class="table-number">Table 1:</span> Integrands and their existence.</caption>

<colgroup>
<col  class="org-left">

<col  class="org-left">

<col  class="org-left">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Function</th>
<th scope="col" class="org-left">Finite as \(x \to \infty\)</th>
<th scope="col" class="org-left">Finite as \(x \to 0\)</th>
</tr>
</thead>
<tbody>
<tr>
<td class="org-left">\(dx/x^2\)</td>
<td class="org-left">Yes</td>
<td class="org-left">No</td>
</tr>

<tr>
<td class="org-left">\(dx/x\)</td>
<td class="org-left">No</td>
<td class="org-left">No</td>
</tr>

<tr>
<td class="org-left">\(dx/x^{1 + \epsilon}\)</td>
<td class="org-left">Yes</td>
<td class="org-left">No</td>
</tr>

<tr>
<td class="org-left">\(dx/x^{1 - \epsilon}\)</td>
<td class="org-left">No</td>
<td class="org-left">Yes</td>
</tr>

<tr>
<td class="org-left">\(dx\)</td>
<td class="org-left">No</td>
<td class="org-left">Yes</td>
</tr>
</tbody>
</table>
</div>
</div>


<div id="outline-container-org34bf0c4" class="outline-2">
<h2 id="org34bf0c4">Special Method of Evaluating a Definite Integral</h2>
<div class="outline-text-2" id="text-org34bf0c4">
<p>
Let \(\displaystyle A = \int_{-\infty}^\infty e^{-x^2} ~dx\).<label for='8eceed6a' class='margin-toggle sidenote-number'></label><input id='8eceed6a' type='checkbox' class='margin-toggle' /><span class='sidenote'>This is the <i>probability integral</i>, so named because it's the (scaled) pdf of the normal distribution.</span></label>
</p>

<p>
This integral exists<label for='2d8b52d8' class='margin-toggle sidenote-number'></label><input id='2d8b52d8' type='checkbox' class='margin-toggle' /><span class='sidenote'>Because \(e^{-x^2} < e^{-\vert x\vert}\) if \(\vert x \vert > 1\) and \(\int_{-\infty}^\infty e^{-\vert x \vert} ~dx = 2\).</span></label>, so its square exists:<label for='6d9d2f88' class='margin-toggle sidenote-number'></label><input id='6d9d2f88' type='checkbox' class='margin-toggle' /><span class='sidenote'>The last line works because the second integral is in a different variable than, and thus constant with respect to, the first.</span></label>
</p>

<p>
\[\begin{aligned}
A^2 &= \int_{-\infty}^\infty e^{-x^2} ~dx \cdot \int_{-\infty}^\infty e^{-y^2} ~dy \\
    &= \int_{-\infty}^\infty \int_{-\infty}^\infty e^{-(x^2 + y^2)} ~dx ~dy.
\end{aligned}\]
</p>

<p>
Now we convert the double integral to polar coordinates. Make the change of variables \(x = r \cos \theta\), \(y = r \sin \theta\):
\[\begin{aligned}
&= \int_0^\infty \int_0^{2\pi} e^{-r^2} \cdot r ~d\theta ~dr \\
&= \int_0^\infty re^{-r^2} \theta\vert_{\theta=0}^{\theta=2\pi} ~dr \\
&= \int_0^\infty 2\pi re^{-r^2} ~dr \\
&= 2\pi \lim_{c \to \infty} \left. -\frac12 e^{-r^2} \right\vert_0^c \\
&= -\pi \lim_{c \to \infty} e^{-c^2} - e^0 \\
&= \pi,
\end{aligned}\]
whereupon we obtain \(A = \sqrt\pi.\)
</p>
</div>
</div>

<div id="outline-container-org2d824f1" class="outline-2">
<h2 id="org2d824f1">Series Evaluation of Definite Integrals<label for='d786cd51' class='margin-toggle sidenote-number'></label><input id='d786cd51' type='checkbox' class='margin-toggle' /><span class='sidenote'>This section was originally titled "Series Solution of Differential Equations," but that doesn't fit.</span></label></h2>
<div class="outline-text-2" id="text-org2d824f1">
<p>
Let's compute \(\displaystyle \int_0^\infty \frac{dx}{e^{nx} + e^{-nx}}.\)<label for='f04d4696' class='margin-toggle sidenote-number'></label><input id='f04d4696' type='checkbox' class='margin-toggle' /><span class='sidenote'>\(= \int_0^\infty \frac12 \operatorname{sech} nx ~dx\).</span></label>
</p>

<p>
We have
</p>

<p>
\[\begin{aligned}
\int_0^\infty \frac{dx}{e^{nx} + e^{-nx}} &= \int_0^\infty \frac{e^{nx}}{e^{2nx} + 1} ~dx\\
\end{aligned}\]
</p>

<p>
This integral can now be evaluated immediately with the substitution \(u = e^{nx}\), but let's do it another way by changing it to a series:
</p>

<p>
\[\begin{aligned}
&= \int_0^\infty e^{nx} \cdot \frac{dx}{1 - (-e^{2nx})}.
\end{aligned}\]
</p>

<p>
Since \(e^{-2nx} < 1\) if \(x > 0\), we have
\[\begin{aligned}
&= \int_0^\infty e^{nx} \left(\sum_{k = 0}^\infty (-1)^k e^{-2knx}\right) ~dx \\
&= \int_0^\infty e^{nx} \left(\sum_{k = 0}^\infty (-1)^k e^{-2knx}\right) ~dx,
\end{aligned}\]
and since the series converges uniformly, we can switch the integral and summation, giving
\[\begin{aligned}
&= \sum_{k = 0}^\infty \int_0^\infty (-1)^k e^{(-2k + 1)nx} ~dx \\
&= \sum_{k = 0}^\infty (-1)^k \left. \frac{1}{(-2k + 1)n} \cdot e^{(-2k + 1)nx} \right\vert_0^\infty \\
&= \sum_{k = 0}^\infty \frac{(-1)^k}{(-2k + 1)n} \\
&= \frac1n \sum_{k = 0}^\infty \frac{(-1)^k}{(-2k + 1)}.
\end{aligned}\]
Then, if we observe that the series is equal to the Taylor series for \(\arctan x\) evaluated at \(x = 1\), we can write
\[
\boxed{\int_0^\infty \frac{dx}{e^{nx} + e^{-nx}} = \frac1n \arctan 1 = \frac1n \cdot \frac\pi4.}
\]
</p>

<p>
A combination of the methods of integration discussed thus far is sufficient to solve any integral which exists. There are still other methods which may be used, and are in many cases easier. We shall discuss some of them later.
</p>
</div>
</div>

<div id="outline-container-orga0ac3da" class="outline-2">
<h2 id="orga0ac3da">Problems</h2>
<div class="outline-text-2" id="text-orga0ac3da">
<ol class="org-ol">
<li>Given \(\displaystyle \int_0^\infty e^{-a^2x^2} ~dx = \frac{\sqrt\pi}{2a},\) prove that \[\displaystyle \int_0^\infty e^{-a^2x^2} \cos bx ~dx = \frac{\pi}{2a} e^{-\frac{b^2}{4a^2}}.\]</li>
<li>Given \(\displaystyle\int_0^\infty e^{-x^2} ~dx = \sqrt\pi/2\), find \(\displaystyle\int_0^\infty x^4e^{-x^2} ~dx\).</li>
<li>Find \(\displaystyle \int_0^\infty \frac{e^{-ay} - e^{-by}}y ~dy\).</li>
<li>Find \(\displaystyle \int_{-\infty}^\infty e^{-\beta x^2 - \frac\alpha{x^2}} ~dx\).</li>
<li>Find \(\displaystyle \int_0^\infty \frac{\sin^2 x}{x^2} ~dx\).</li>
<li>Prove \(\delta(ax) = \frac1a \delta(x)\) for \(a > 0\), in the sense that they have equal integrals over any interval.</li>
<li>Find \(x\delta''(x)\).</li>
<li>Integrate \(\displaystyle \int_0^\infty \frac{\cos mx}{(1 + x^2)^2} ~dx.\)<label for='8b1420f2' class='margin-toggle sidenote-number'></label><input id='8b1420f2' type='checkbox' class='margin-toggle' /><span class='sidenote'>Hint: Use the previous derivation to obtain \(\int_0^\infty \frac{\cos mx}{k^2 + x^2} ~dx.\) Then differentiate with respect to \(k\).</span></label></li>
<li>Show that \(\displaystyle \int_0^\infty \cos(x^2) ~dx = \int_0^\infty \sin(x^2) ~dx = \frac12 \sqrt{\frac\pi2}\).<label for='d88bade8' class='margin-toggle sidenote-number'></label><input id='d88bade8' type='checkbox' class='margin-toggle' /><span class='sidenote'>These are called the <i>Fresnel integrals</i>, and they are demonstrated beautifully in the Cornu spiral.</span></label></li>
<li>Integrate \(\displaystyle \int_0^\infty \frac{x}{e^{mx} - e^{-mx}} ~dx.\)</li>
<li>Let \(\displaystyle \int_0^\infty \frac{\sin x}{\sinh ax} ~dx = S(a)\).
<ol class="org-ol">
<li>Find \(S(1)\) to 3 significant figures.</li>
<li>Find approximate expressions for \(S(a)\) for large and small \(a\).</li>
</ol></li>
</ol>

</article>
</div>
</div>
</div>
</body>
</html>
