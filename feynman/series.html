<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2020-05-16 Sat 08:57 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Feynman Mathematical Methods: Series</title>
<meta name="generator" content="Org mode">
<meta name="author" content="James Keck, transcribed by SVM">
<link rel="stylesheet" type="text/css" href="../assets/css/post-style.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2020 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">Feynman Mathematical Methods: Series</h1>
</header><nav id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgc426cea">Abel Summability</a></li>
<li><a href="#org25387e2">Summation of Series by Integration and Differentiation</a></li>
<li><a href="#orgf0de35b">Summation of Series by Adding Terms</a></li>
<li><a href="#org6d54e5f">Magnitude of Quantities</a></li>
<li><a href="#org075a143">De L'Hospital's Rule</a></li>
<li><a href="#orgb8a1e3e">Problems</a></li>
</ul>
</div>
</nav>
<article>

<div id="outline-container-orgc426cea" class="outline-2">
<h2 id="orgc426cea">Abel Summability</h2>
<div class="outline-text-2" id="text-orgc426cea">
<p>
Consider the series \(1 - 2 + 3 - \dots.\) Convert it to a power series in \(x\) as follows: \(S(x) = 1 - 2x + 3x^2 - \dots\)
</p>

<p>
Sum this power series:
</p>

<p>
\[\begin{aligned}
\int S(x) ~dx &= x - x^2 + x^3 - \dots = \frac{x}{1 + x} \\
S(x) &= \frac{d}{dx}\left[\frac{x}{1 + x}\right] = \frac1{(1 + x)^2}
\end{aligned}\]
</p>

<p>
The Abel sum of the original series is defined as the \(\lim_{x \to 1-} S(x)\).
</p>

<p>
\(
\text{Abel sum} = \lim_{x \to 1-}\left[\frac1{(1 + x)^2}\right] = \frac14
\)
</p>

<p>
In general, oscillating series can be summed, but the order of the terms must not be changed.<label for='3cac57ff' class='margin-toggle sidenote-number'></label><input id='3cac57ff' type='checkbox' class='margin-toggle' /><span class='sidenote'>Because the <a href="https://en.wikipedia.org/wiki/Riemann_series_theorem">Riemann rearrangement theorem</a> says that if you get to reorder the terms you can make an oscillating series add up to anything.</span></label>
</p>
</div>
</div>

<div id="outline-container-org25387e2" class="outline-2">
<h2 id="org25387e2">Summation of Series by Integration and Differentiation</h2>
<div class="outline-text-2" id="text-org25387e2">
<p>
\[\begin{aligned}
S &= 1 + \frac14 + \frac19 + \frac1{16} + \dots\\
S(x) &= x + \frac{x^2}4 + \frac{x^3}9 + \dots \\
S'(x) &= 1 + \frac{x}2 + \frac{x^2}3 + \dots \\
&= \frac{-\ln(1 - x)}x \\
\\
S(x) &= -\int \frac{\ln(1 - x)}x ~dx
\end{aligned}\]
</p>

<p>
One should know and recognize the series for the following expressions:
</p>

<ol class="org-ol">
<li>\(\frac{1}{1 - x} = 1 + x + x^2 + x^3 + \dots \)</li>
<li>\(-\ln(1 - x) = x + \frac{x^2}2 + \frac{x^3}3 + \dots \)</li>
<li>\(\tan^{-1}x = x - \frac{x^3}3 + \frac{x^5}5 - \frac{x^7}7 + \dots \)</li>
<li>\(e^x = 1 + \frac{x}{1!} + \frac{x^2}{2!} + \frac{x^3}{3!} + \dots \)</li>
<li>\(\sin x = x - \frac{x^3}{3!} + \frac{x^5}{5!} - \dots \)</li>
<li>\(\cos x = 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - \dots \)</li>
<li>\(\frac1{(1 + x)^2} = 1 - 2x + 3x^2 - \dots \)</li>
<li>\(\frac1{\sqrt{1 + x}} = 1 - \frac12 x + \frac38 x^2 - \frac{15}{48}x^3 + \dots \)<label for='9ebf3ba9' class='margin-toggle sidenote-number'></label><input id='9ebf3ba9' type='checkbox' class='margin-toggle' /><span class='sidenote'>The coefficient of \(x^n\) is \(\frac{(2n - 1)!!}{(2n)!!}\).</span></label></li>
</ol>


<p>
A useful relation: \( e^{i\theta} = \cos \theta + i \sin \theta \)
</p>
</div>
</div>


<div id="outline-container-orgf0de35b" class="outline-2">
<h2 id="orgf0de35b">Summation of Series by Adding Terms</h2>
<div class="outline-text-2" id="text-orgf0de35b">
<p>
Consider the series \(1 + \frac14 + \frac19 + \dots.\) We can sum it as follows: \(1.000 + .250 + .111 + .0625 + \dots\)
</p>

<p>
By taking enough terms we can obtain any desired accuracy. If the series is slow to converge it is sometimes possible to replace the higher terms by an integral:
</p>


<figure>
<img src="figs/baselplot.png" alt="baselplot.png">

<figcaption><span class="figure-number">Figure 1: </span>The Basel plot: approximating \(\pi^2/6\) by \(\int 1/x^2 ~dx\)</figcaption>
</figure>

<p>
Examination of the above diagram shows that
</p>

<p>
\[\begin{aligned}
\frac1{3^2} + \frac1{4^2} + \dots &\approx \int_{2 \frac12}^\infty \frac1{x^2} ~dx \\
&\approx \frac1{2.5} = .4.
\end{aligned}\]
</p>

<p>
The sum of our series is then<label for='cb5fc464' class='margin-toggle sidenote-number'></label><input id='cb5fc464' type='checkbox' class='margin-toggle' /><span class='sidenote'>Approximately; the exact value is \(\pi^2/6 = 1.6449...\)</span></label> \(1.000 + .250 + .400 \approx 1.650.\)
Since the error will be principally in the first terms some idea of its magnitude may be gotten by looking at the next best approximation:
</p>

<p>
\[\begin{aligned}
\frac1{4^2} + \frac1{5^2} + \dots &\approx \int_{3 \frac12}^\infty \frac1{x^2} ~dx \\
&\approx .286
\end{aligned}\]
</p>

<p>
The new sum is \(1.000 + .250 + .111 + .286 \approx 1.647.\)
We see that we have made an error of only \(.003\). We can guess from this that the third figure is probably about 6.
</p>
</div>
</div>

<div id="outline-container-org6d54e5f" class="outline-2">
<h2 id="org6d54e5f">Magnitude of Quantities</h2>
<div class="outline-text-2" id="text-org6d54e5f">
<p>
The following quantities are arranged in the order of their rate of rise for large \(x\)<label for='dc4fe2bf' class='margin-toggle sidenote-number'></label><input id='dc4fe2bf' type='checkbox' class='margin-toggle' /><span class='sidenote'>I prefer to write \(1/n\) for \(\epsilon\), or at least I did when I taught calculus.</span></label>:
\[1 < \ln x < x^\epsilon < x < x^n < e^x \]
</p>

<p>
To ascertain their behavior for small \(y\), make the substitution \(y = \frac1x\).
</p>

<p>
The following approximations hold for small \(x\).
</p>

<p>
\[\begin{aligned}
\sin x &\sim x \\
\cos x &\sim 1 - \frac{x^2}2 \\
\tan^{-1}x &\sim x \sim \tan x \\
\sinh x &\sim x \\
e^x &\sim 1 + x \\
(1 + x)^n &\sim 1 + nx \\
(1 + x)^{-1/2} &\sim 1 - \frac12 x
\end{aligned}\]
</p>
</div>
</div>

<div id="outline-container-org075a143" class="outline-2">
<h2 id="org075a143">De L'Hospital's Rule</h2>
<div class="outline-text-2" id="text-org075a143">
<p>
If as \(x \to a\), \(f(x)\) and \(g(x) \to 0\), then
\[
\lim_{x\to a} \frac{f(x)}{g(x)} = \lim_{x \to a} \frac{f'(x)}{g'(x)}.
\]
</p>

<p>
Example:
\[
\lim_{x \to 0} \frac{\tan x}{[(1 + x)^3 - 1]} = \lim_{x \to 0} \frac{\sec^2 x}{3(1 + x)^2} = \frac13.
\]
</p>
</div>
</div>

<div id="outline-container-orgb8a1e3e" class="outline-2">
<h2 id="orgb8a1e3e">Problems</h2>
<div class="outline-text-2" id="text-orgb8a1e3e">
<ol class="org-ol">
<li>Sum:</li>
</ol>
<p>
\(\begin{gathered}
\frac1{2!} + \frac2{3!} + \frac3{4!} + \dots \\
\frac{1}{1 \cdot 2} + \frac1{2 \cdot 3} + \frac1{3 \cdot 4} + \dots
\end{gathered}\)
</p>
<ol class="org-ol">
<li>Find</li>
<li>\(\lim_{x \to 0} \frac{\sin x}{e^{3x} - 1}\)</li>
<li>\(\lim_{x \to 0} \frac{\ln(\frac12 + \frac1{2\sqrt{1+x^2}})}{e^{x^2}-\cos x^2}\)</li>
</ol>

</article>
</div>
</div>
</div>
</body>
</html>
