<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2020-05-16 Sat 08:57 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Feynman Mathematical Methods: Elementary Mathematics</title>
<meta name="generator" content="Org mode">
<meta name="author" content="James Keck, transcribed by SVM">
<link rel="stylesheet" type="text/css" href="../assets/css/post-style.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2020 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">Feynman Mathematical Methods: Elementary Mathematics</h1>
</header><nav id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org7dae15c">Equations</a>
<ul>
<li><a href="#org7c76af9">Example: Interpolation</a></li>
<li><a href="#org0da5157">Example: Iteration</a></li>
<li><a href="#org7b60e6f">Example: Newton's method</a></li>
</ul>
</li>
<li><a href="#orga98dfd8">Problems</a></li>
<li><a href="#orgfa696fa">Power Series</a>
<ul>
<li><a href="#org3ecc862">Summation by Integration and Differentiation</a></li>
<li><a href="#orgda02e58">Convergence: Ratio Test</a></li>
</ul>
</li>
</ul>
</div>
</nav>
<article>

<div id="outline-container-org7dae15c" class="outline-2">
<h2 id="org7dae15c">Equations</h2>
<div class="outline-text-2" id="text-org7dae15c">
<p>
The simplest way to solve any equation - except linear or quadratic equations - is by <i>trial and error</i>.
</p>
</div>

<div id="outline-container-org7c76af9" class="outline-3">
<h3 id="org7c76af9">Example: Interpolation</h3>
<div class="outline-text-3" id="text-org7c76af9">
<p>
\(\frac{1}{1 + x^2} = 2x\)
</p>

<p>
\[\begin{array}{|l|l|l|l|}
\mathbf{x} & \textbf{LHS} & \textbf{RHS} & \textbf{Difference} \\
0 & 1 & 0 & 1 \\
1 & .5 & 2 & -1.5 \\
.4 & .862 & .8 & .062 \\
.
\end{array}\]
</p>
</div>
</div>

<div id="outline-container-org0da5157" class="outline-3">
<h3 id="org0da5157">Example: Iteration</h3>
<div class="outline-text-3" id="text-org0da5157">
<p>
Solve the equation for \(x\).
</p>

<p>
\(x = \frac12(1/1 + x^2)\)
</p>

<p>
\[\begin{array}{rl}
\textbf{trial } \mathbf{x} & \textbf{result } \mathbf{x} \\
0  \to & .5 \\
.5  \swarrow & .4 \\
.4  \swarrow & .431 \\
\dots \swarrow & \dots
\end{array}\]
</p>

<p>
If the equation is solved in the form \(x = \sqrt{\frac1{2x} - 1}\), however, the results diverge. If the results do diverge, solving the equation in the inverse manner<label for='11fa61ee' class='margin-toggle sidenote-number'></label><input id='11fa61ee' type='checkbox' class='margin-toggle' /><span class='sidenote'>That is to say, inverting the side that is not \(x\).</span></label> will make them converge.
</p>
</div>
</div>


<div id="outline-container-org7b60e6f" class="outline-3">
<h3 id="org7b60e6f">Example: Newton's method</h3>
<div class="outline-text-3" id="text-org7b60e6f">
<p>
Solve the equation in the form \(f(x) = 0\).
</p>

<p>
Let \(x_1\) be the first trial value; then the second trial value is given by the relation \(x_2 = x_1 - f(x_1)/f'(x_1)\). This procedure is equivalent to extrapolating back along the tangent. If the curve is full of wiggles things may diverge.
</p>

<p>
\(\begin{gathered}
f(x) = 1/(1 + x^2) - 2x = 0\\
f'(x) = -2x/(1 + x^2)^2 - 2
\end{gathered}\)
</p>

<p>
\(\begin{array}{|l|l|l|}
\mathbf{x} & \mathbf{f(x)} & \mathbf{f'(x)} \\
0 & 1 & 2 \\
.5 & 2 & \dots \\
\dots & \dots & \dots
\end{array}\)
</p>
</div>
</div>
</div>


<div id="outline-container-orga98dfd8" class="outline-2">
<h2 id="orga98dfd8">Problems</h2>
<div class="outline-text-2" id="text-orga98dfd8">
<ul class="org-ul">
<li>Find the first positive root of \(e^{-x} = \cos x\) by all three methods to 2 decimals. Estimate higher roots.</li>
<li>Prove \(1 + x + x^2 + x^3 + \dots = 1/(1 - x)\).</li>
<li>Sum \(1 + \alpha \cos \theta + \alpha^2 \cos 2\theta + \alpha^3 \cos 3\theta + \dots.\)</li>
</ul>

<div class="solution-wrapper">
<label class="show-solution" for="solution-cos-power-series">Solution</label>
<input type="checkbox" class="show-solution" id="solution-cos-power-series"/>
<div class="solution">
<p>
Assume \(\vert \alpha\vert < 1\). Then we have:
</p>

<p>
\[\begin{aligned}
\sum_{n = 0}^\infty \alpha^n \cos n\theta &= \Re\left[\sum_{n = 0}^\infty \alpha^n (\cos n\theta + i \sin n\theta)\right] \\
&= \Re\left[\sum_{n=0}^\infty \alpha^n e^{in \theta}\right] \\
&= \Re\left[\sum_{n=0}^\infty (\alpha e^{i\theta})^n \right] \\
&= \Re\left[\frac1{1 - \alpha e^{i\theta}}\right]\\
&= \Re\left[\frac1{1 - \alpha \cos\theta - \alpha i\sin\theta}\right]\\
&= \Re\left[\frac{1 - \alpha \cos\theta + \alpha i\sin\theta}{(1 - \alpha \cos \theta)^2 - (\alpha i \sin \theta)^2} \right]\\
&= \frac{1 - \alpha \cos \theta}{(1 - \alpha \cos \theta)^2 + (\alpha \sin \theta)^2} \\
&= \frac{1 - \alpha \cos \theta}{1 - 2\alpha\cos\theta + \alpha^2}.
\end{aligned}\]
</p>
</div>
</div>
</div>
</div>

<div id="outline-container-orgfa696fa" class="outline-2">
<h2 id="orgfa696fa">Power Series</h2>
<div class="outline-text-2" id="text-orgfa696fa">
</div>
<div id="outline-container-org3ecc862" class="outline-3">
<h3 id="org3ecc862">Summation by Integration and Differentiation</h3>
<div class="outline-text-3" id="text-org3ecc862">
<p>
\[\begin{aligned}
  1(x) - \frac12 x^2 + \frac13 x^3 - \frac14 x^4 + \dots &= S(x) \\
1 - x + x^2 - x^3 + x^4 + \dots = S'(x) \\
S'(x) = \frac1{1+x}\\
S(x) = \ln(1 + x) + C
\end{aligned}\]
</p>

<p>
Evaluate constant for \(x = 0\):
</p>

<p>
\[\begin{aligned}
S(0) &= \ln 1 + C \\
0 &= 0 + C \\
S(1) &= \ln 2 \approx .69315
\end{aligned}\]
</p>
</div>
</div>
<div id="outline-container-orgda02e58" class="outline-3">
<h3 id="orgda02e58">Convergence: Ratio Test</h3>
<div class="outline-text-3" id="text-orgda02e58">
<p>
If the \(\lim_{m \to \infty} x_{m + 1}/x_m < 1\) the series converges.
</p>

</article>
</div>
</div>
</div>
</div>
</body>
</html>
